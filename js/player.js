
/**
 * vboss.player allows playback of assets using HTML5 or flash, depending on browser capabilites
 *
 * @param {guid} applicationId the ID of the player to use.  If you do not have this value contact VBrick Support
 * @param {string} containerId the ID of the div in which to place player content.  The node with this ID will be REPLACED by the video player.
 * @param {object} options additional configuration options, which may be:
 *		@param {boolean=} preferHTML5 If set to true then use HTML5 player on browsers that support it, even if they support flash as well.
 *		@param {string=} width css string for width of player (i.e. 640px). default is fill container
 *		@param {string=} height css string for height
 *		@param {boolean=} autoplay start video on load
 *		@param {boolean=} automute start video silent
 *		@param {string=} bgcolor background color of player
 *		@param {string=} wmode (flash only) wmode to use (transparent, opaque or window)
 *		@param {mixed=} theme (html5 only) set the encoding theme for account.  See versionSelect class for options
 *
 */
vboss.player = function(applicationId, containerId, _options) {
	
	var options = $.extend({}, this.defaultOptions, _options),
		support = this.detectCapabilities(),
		playerClass;
	
	// use flash if able
	if (support.flash) {
		// ...except if html5 is preferred
		if (options.preferRTMP && support.rtmp) {
			playerClass = vboss.player.rtmp;
		} else if (options.preferHTML5 && support.mp4) {
			playerClass = vboss.player.html5;
		} else {
			playerClass = vboss.player.flash;
		}
	} else if (support.html5) {
		options.flashFallback = false; // flash isn't possible
		playerClass = vboss.player.html5;
		
	} else if (options.smartTagFallback) {
		playerClass = vboss.player.smartTag;
	
	}
	
	// return selected class if found
	if (playerClass) {
		return new playerClass(applicationId, containerId, options);
	}
	
	// fallback is to just display a preview and direct link to MP4
	this.applicationId = applicationId;
	this.containerId = containerId;
	this.options = $.extend({}, this.defaultOptions, options);
	
	vboss.util.handleError('browser does not support playback. falling back to direct link');
		
	return this;
};

vboss.player.prototype.defaultOptions = {
	preferHTML5: false,
	smartTagFallback: true,
	flashFallback: true, // flash fallback for HTML5 player
	width: '100%',
	height: '100%',
	autoplay: false,
	automute: false,
	bgcolor: '#000000',
	wmode: 'transparent',
	// change according to account settings - see vboss.util.versionSelect
	theme: 'standard',
	// use default version unless specified -- avoids delays when theme isn't set
	version: 'default'
};

/**
 * load given asset
 *
 * @param {guid} assetId id of asset to play
 * @param {object=} options
 */
vboss.player.prototype.load = function(assetId, options) {
	options = $.extend({}, this.defaultOptions, options);

	this.assetId = assetId;
	
	$('#'+this.containerId).html([
		'<div style="position:relative;">',
			'<a href="http://cdn9.fliqz.com/', assetId, '.mp4">',
				'<img src="http://previews.fliqz.com/', assetId, '.jpg" alt="Click to play"',
					' style="width:', options.width, ';height:', options.height, '" />',
				'<img style="position:absolute;left:50%;top:50%;z-index:1000;border:0px none;margin:-25px 0 0 -145px;max-width:100%;"',
				' src="http://images.fliqz.com/b7bc4f9192b34cf1a9566305fd94a4ae.png" alt="Play" />',
			'</a>',
		'</div>'
	].join(''));
	
	// return a promise for compatibility with other player types
	var promise = new $.Deferred();
	promise.resolve();
	
	return promise;
	
};


/**
 * detect if browser supports html5 and flash
 */
vboss.player.prototype.detectCapabilities = function() {
	var support = {};

	/* use SWFObject to detect if flash is available */
	if((typeof swfobject != 'undefined') && swfobject) {
		support.flash = swfobject.hasFlashPlayerVersion('9.0.115');
		support.rtmp = swfobject.hasFlashPlayerVersion('10.0.000');
	} else {
		support.flash = false;
		if('ActiveXObject' in window) {
			try {
				support.flash = !!(new ActiveXObject('ShockwaveFlash.ShockwaveFlash'));
			} catch(e){ 
				support.flash = false;
			}
		} else {
			support.flash = !!navigator.mimeTypes['application/x-shockwave-flash'];
		}
		
		// can't detect version using this method, so just assume high enough
		support.rtmp = support.flash;
	}
	
	// html5 support
	var node = document.createElement('video');
	support.html5 = (typeof node.canPlayType != 'undefined');
	if(!support.html5) return support;
	
	// mp4 support (check if firefox)
	support.mp4 = node.canPlayType('video/mp4').replace(/no/,'') != '';
	if(!support.mp4) return support;
	
	// if andoroid pre-2.3 then can't handle html5 properly
	var ua = window.navigator.userAgent.toLowerCase();
	support.html5controls = !ua.match('android 2\.[12]');
			
	return support;
};


function testFlash() {
	
}