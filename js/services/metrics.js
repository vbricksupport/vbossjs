/**
 * VBOSS On-Demand Metrics Service
 *
 * metrics constructor
 * this class allows you to use a native HTML5 player with VBOSS On-Demand videos.
 * it allows you to initiate a play event and retrieve the URL of a video asset MP4,
 * and allows logging of playback events (pause, seek, etc) with the VBOSS Metrics Service.
 * 
 * @param {guid} applicationId REQUIRED - the REST Search Services ApplicationID / PlayerID for 
 * 		your account.  The asset version of the video (HD, SD, mobile/low-bitrate) will 
 * 		correspond to the asset version for the given PlayerID
 */
vboss.metrics = function(applicationId) {
	if(!(this instanceof vboss.metrics)) return new vboss.metrics(applicationId);
	
	this.applicationId = applicationId;
	this.sessionId = null;
	this.assetId = null;
	this.assetUrl = null;
	
	this.el = null; // for html5 support
	
	this._startTime = new Date(); // time metrics session started
	
	return this;
	
};
 

/**
 * constants
 * baseUri: - url for POST requests
 * metricsUpdateInterval: rate at which 'continue' events are sent to server.
 */

vboss.metrics.prototype.ERROR_MESSAGES = {
	400: 'The input parameters are invalid or malformed.',
	403: 'The Account owning the Application has been suspended.',
	404: 'The ApplicationID, SessionID or AssetID is unknown.',
	500: 'An unrecoverable server side error has occurred.'
};

vboss.metrics.prototype.baseUri = (/https/.test(window.self.location.protocol) ? 'https:' : 'http:') + '//services.fliqz.com/metrics/20090201/MetricsService.svc/';
vboss.metrics.prototype.metricsUpdateInterval = 10;

/**
 * internal function - make ajax calls
 */
vboss.metrics.prototype.ajax = function(url, params, callback, errorCallback) {
	var ERROR_MESSAGES = this.ERROR_MESSAGES,
			request = $.ajax({
		url: url + params,
		type: 'POST',
		cache: false,
		crossDomain: true,
		dataType: 'xml',
		converters: { 'text xml': $.parseXML }
	}).pipe(null, function(xhr) { // provide useful error message on failure
			var statusCode = xhr.status;
			var message = ERROR_MESSAGES[statusCode] || xhr.statusText;
			
			return {
				xhr: xhr,
				status: statusCode,
				message: message
			};
			
	});
	
	if($.isFunction(callback)) request.done(callback);
	if($.isFunction(errorCallback)) request.fail(errorCallback);
	
	return request;
}; 
/**
 * createSession - request a new metrics session from VBrick.  returns sessionId to callback
 * optionally include callbacks to be called when load is complete or an error occurs
 */
vboss.metrics.prototype.createSession = function(callback, errorCallback) {
	var _this = this;
	
	// construct the URL with REST parameters
	var url = [
			this.baseUri,
			'applications/', this.applicationId,
			'/sessions/'
		].join('');
	
	// include additional information
	var params = [
			'?offset=', this._offsetTime(), // time from player load
			'&ct=', (new Date()).toISOString(), // current time in ISO format
			'&uri=', document.location.href // the host page on which video is embedded
		].join('');
	
	var request = this.ajax(url, params)
		.pipe(function(response) {
			_this.sessionId = $('fqzsm\\:sid, sid', response).text(); // xml selector (fix for differing browser compatibility with namespaces
			_this.serverTime = $('fqzsm\\:st, st', response).text(); // returns server time -- not needed for any subsequent requests
	
			return _this.sessionId;
		});
		
	if($.isFunction(callback)) request.done(callback);
	if($.isFunction(errorCallback)) request.fail(errorCallback);
	
	return request;
	
};

/**
 * load - log a play event and retrieve media asset URL
 * @param {guid} assetId REQUIRED - specify the asset to play
 * @param {function=} callback - function called on successful load.  returns url of asset MP4
 * @param {function=} error - function called on load error. returns server response
 * @returns {jQuery Deferred} deferred object for use with .done and .fail
 */
vboss.metrics.prototype.load = function(assetId, callback, errorCallback) {
	var _this = this;
	
	this.assetId = assetId;
	
	// construct the URL with REST parameters
	var url = [
			this.baseUri,
			'applications/', this.applicationId,
			'/sessions/',  this.sessionId,
			'/assets/', this.assetId,
			'/events/', 'load/'
		].join('');
		
	// include additional information
	var params = [
			'?offset=', this._offsetTime()
		].join('');
	
	// make ajax call to server
	var request = this.ajax(url, params)
		.pipe(function(response) {
			_this.assetUrl = $('fqzsm\\:u, u', response).text(); // retrieve MP4 url
			return _this.assetUrl;
		});
	
	if($.isFunction(callback)) request.done(callback);
	if($.isFunction(errorCallback)) request.fail(errorCallback);
	
	return request;
};

/**
 * registerEvent -- send metrics events (play, pause, seek, etc) to server
 * pulls in current time / video time using private functions
 * @param eventType REQUIRED - see addEventHandlers for tracked events
 *
 */
vboss.metrics.prototype.registerEvent = function(eventType) {
	// construct the URL with REST parameters
	var url = [
			this.baseUri,
			//'applications/', this.applicationId,
			'sessions/',  this.sessionId,
			'/assets/', this.assetId,
			'/events/', eventType, '/'
		].join('');
		
	// include additional information
	var params = [
			'?offset=', this._offsetTime(),
			'&assetTime=', this._currentTime(),
		].join('');
	
	// make ajax call to server
	return this.ajax(url, params);
	
};

// private function - get time from initial metrics load
vboss.metrics.prototype._offsetTime = function() {
	var time = (new Date() - this._startTime)/1000;
	return "PT" + Math.round(time) + "S";
};

// private function - get current video playback time
vboss.metrics.prototype._currentTime = function() {
	if(!this.el) return null;
	return "PT" + Math.round(this.el.currentTime) + "S";
};

/**
 * addEventHandlers -- register metrics event on HTML5 audio/video element
 * this will register all metrics events with the supplied element
 *
 * @param {jQuery selector} element selector for element to bind.  may be selector or DOM element
 */
vboss.metrics.prototype.addEventHandlers = function(element) {
	var _this = this;
	var lastTimeUpdate = 0; // used by timeupdate to throttle update events
	var seeking = false; // used by seeking to throttle seek events
	
	var $el = $(element);
	
	// store element reference
	_this.el = $el[0];
	
	if(!_this.el || _this.el.currentTime==undefined) throw "invalid element or no element defined";
	
	// on these HTML5 events a metrics event will be sent to server
	
	$el.on('play', function() {
		_this.registerEvent('play');
	});
	
	$el.on('pause', function() {
		if(_this.el.ended === true) return; // ignore pause event at end of video
		_this.registerEvent('pause');
	});
	
	$el.on('resume', function() {
		_this.registerEvent('play');
	});
	
	$el.on('ended', function() {
		_this.registerEvent('complete');
	});
	
	$el.on('seeking', function() {
		if(seeking) return false; // avoid duplicate events

		seeking = true;				
		_this.registerEvent('dstart');
		
	});
	
	$el.on('seeked', function() {
		seeking = false;
		_this.registerEvent('dend');
	});
	
	$el.on('timeupdate', function() {
		if(_this._seeking) return false; // don't send updates during seek
		
		var time = _this.el.currentTime;
	
		// send an update event only if interval between updates has passed
		if(lastTimeUpdate + _this.metricsUpdateInterval > time) return false;
		
		lastTimeUpdate = time;
		_this.registerEvent('continue');
		
	});
	
	$el = null;
	
	return _this;

};
