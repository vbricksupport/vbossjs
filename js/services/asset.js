/************************************************/
/**
 * Asset - wrapper class for asset entries from REST Search Services API
 */
vboss.asset = function(data) { 
	var _this = this;
	// ensure new instance
	if(!this instanceof vboss.asset) return new vboss.asset(data);
			
	// create asset from xml if provided (or array of assets if xml includes multiple entries
	if(data && $.isXMLDoc(data)) return this.populateFromXml(data);
	
	// if xml not included return empty asset
	this.id = null;
	this.title = '';
	this.description = '';
	this.keywords = '';
	this.categories = {};
	this.link = '';
	this.date = null;
	this.preview = '';
	this.player = {
		applicationId: null,
		width: null,
		height: null
	};
	
	this.thumbnails = null;
	this.metrics = {
		duration: 0,
		state: '',
		plays: 0,
		shares: 0,
		votes: 0,
		rating: 0
	};
	
	return this;
};

/**
 * converts api results to javascript object
 * if xml includes multiple asset entries then returns an array of objects
 */
vboss.asset.prototype.populateFromXml = function(xml) {
	// use node if given <item>, otherwise find all item nodes
	var $node = $(xml).is('item') ? $(xml) : $('item', xml);
	
	if($node.length == 0) {
	
		var itemTotal = _node('itemTotal', 'fzat').text();

		if(itemTotal == '' || itemTotal === "0") {
			return new vboss.asset(); // return empty asset
		}
		
	// return array if multiple items in xml
	} else if($node.length > 1) {
		return $.map($node, function(item) { return new vboss.asset(item); });		
	}
	
	// private helper function - browser-agnostic search for namespaced xml nodes
	function _node(key, namespace) {
		if(namespace) return $([namespace, '\\:', key, ', ', key].join(''), $node[0]);
		else return $(key, $node[0]);
	};
	
	var item = new vboss.asset();
	
	// get Metadata
		
	item.id = _node('guid').text();
	item.title = _node('title').text();
	item.description = _node('description').text();
	item.keywords = _node('keywords','media').text();
	item.link = _node('link').text();
	item.date = new Date(_node('pubDate').text());
	
	// include any category values
	item.categories = {};
	_node('category').each(function(i, category) {
		var key = ($(category).attr('domain') || '').replace(/(.*):/, '');
		item.categories[key] = $(category).text();
	});
	
	// get Player Info
	var playerNode = _node('player', 'media');
	item.player.applicationId = playerNode.attr('url').replace(/.*\/(.*)\.swf.*/, '$1');
	item.player.width = playerNode.attr('width');
	item.player.height = playerNode.attr('height');
	
	// preview and thumbnails
	var thumbnailNodes = _node('thumbnail', 'media');
	item.thumbnails = $.map(thumbnailNodes, function(thumbnail, i) { 
		var $thumb = $(thumbnail);
		
		// check if default preview
		if($thumb.attr('fzat:isDefault') == 'true') {
			item.preview = $thumb.attr('url');
			return null; // don't include in thumbnail results
		}
			
		return {
			url: $thumb.attr('url'),
			time: $thumb.attr('time')
		}
	});
	
	// metrics
	item.metrics = {};
	
	// video information
	var contentNode = _node('content', 'media');
	item.metrics.duration = parseInt(contentNode.attr('duration')) || 0;
	item.metrics.state = contentNode.attr('fzat:state');
	item.metrics.approved = contentNode.attr('fzat:approved');
	
	// metrics information
	var metricsNode = _node('metrics', 'fzat');
	item.metrics.plays = parseInt(metricsNode.attr('plays')) || 0;
	item.metrics.shares = parseInt(metricsNode.attr('shares')) || 0;
	item.metrics.votes = parseInt(metricsNode.attr('votes')) || 0;
	item.metrics.ratings = parseInt(metricsNode.attr('ratings')) || 0;
	// 0.5 for backwards compatibility.  Previously always showed 0
	item.metrics.rating = item.metrics.ratings;
	
	return item;
};

/**
 * embed video in the specified div using the SmartTag
 * 
 * @param {string} containerId video content will be loaded into div with specified id
 * @param {object=} options hash of additional embed settings, which may be: (optional)
 *		@param {string=} width css string for width of player (i.e. 640px). default is fill container
 *		@param {string=} height css string for height
 *		@param {boolean=} autoplay start video on load (flash-only)
 *		@param {boolean=} automute start video silent (flash-only)
 *		@param {string=} bgcolor background color of player (flash-only)
 *
 */
vboss.asset.prototype.embed = function(containerId, options) {
	options = options || {};
	
	var protocol = /https/.test(document.location.protocol) ? 'https:' : 'http:',
			smartTagUrl = protocol + '//services.fliqz.com/smart/20100401/' + 
				'applications/' + this.player.applicationId +
				'/assets/' + this.id +
				'/containers/' + containerId +
				'/smarttag.js';
						
	return $.ajax({
		url: smartTagUrl,
		data: {
			width: options.width || '100%',
			height: options.height || '100%',
			autoPlayback: options.autoplay || false,
			audioMute: options.automute || false,
			bgcolor: options.bgcolor || '#000000'		
		},
		dataType: 'script',
		cache: 'true'
	});
	
};