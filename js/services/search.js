/**
 * vboss.search is a wrapper around the VBOSS On-Demand REST Search Services API
 */

vboss.search = {

	/**
	 * private function to handle ajax requests
	 *
	 * @param {string} url The url to load
	 * @param {object=} data additional query parameters (optional)
	 * @param {function=} completeCallback function called when done (optional)
	 * @param {function=} errorCallback function called on error (optional)
	 */
	ajax: function(url, data, completeCallback, errorCallback) {
		var request = $.ajax({
			url: url,
			data: data,
			type: 'GET',
			cache: true,
			crossDomain: true,
			dataType: 'xml',
			converters: { 'text xml': $.parseXML }
		}).pipe(function(xml) { 
			
			var itemTotal = $('itemTotal, fzat\\:itemTotal', xml).text();
			var results;
			
			if(itemTotal==0) {
				vboss.util.handleError('search: no results');
				
				results = [];
				results.itemTotal = 0;
				results.pages = 0;
				results.page = 0;
				
				return results;
			}
			
			results = new vboss.asset(xml);
			
			// if a collection of assets then augment array with information about collection, and include pagination functions
			if(itemTotal>1) {

				results.itemTotal = itemTotal;
				results.pages = $('pages, fzat\\:pages', xml).text();				
				results.page = ($('[rel=self]', xml).attr('href') || '').match(/n=(\d+)/) ? RegExp.$1 : '1';
				
				var prev = $('[rel="previous"]', xml).attr('href'),
					next = $('[rel="next"]', xml).attr('href'),
					first = $('[rel="first"]', xml).attr('href'),
					last = $('[rel="last"]', xml).attr('href');
							
				if(prev) results.prev = function(callback, errorCallback) {	
					return vboss.search.ajax(prev, null, callback, errorCallback); 
				};
				
				if(next) results.next = function(callback, errorCallback) {	
					return vboss.search.ajax(next, null, callback, errorCallback); 
				};
				
				if(first) results.first = function(callback, errorCallback) {	
					return vboss.search.ajax(first, null, callback, errorCallback); 
				};
				
				if(last) results.last = function(callback, errorCallback) {	
					return vboss.search.ajax(last, null, callback, errorCallback); 
				};
		
			}
			
			return results;
		}, function(xhr) { // on error provide useful message
			var statusCode = xhr.status;
			var message = '';
			switch(statusCode) {
				case 403:
					message = 'The Account is suspended or Application does not allow API access.';
				break;
				case 404:
					message = 'The Application or Asset is unknown, or the URI used to access the service is incorrect.';
				break;
				case 500:
					message = 'An unrecoverable server side error has occurred.';
				break;
				default:
					message = xhr.statusText;
			}
			
			return {
				xhr: xhr,
				status: statusCode,
				message: message
			};
			
		});
		
		if($.isFunction(completeCallback)) request.done(completeCallback);
		if($.isFunction(errorCallback)) request.fail(errorCallback);
		
		return request;
	},

	/* url for REST Service */
	baseUri: (/https/.test(window.self.location.protocol) ? 'https:' : 'http:') + '//services.fliqz.com/assets/20090819/AssetSearch.svc/',

	/**
	 * default search options - see documentation for available values
	 */
	defaultOptions: {
		fields: 'all',
		sort: 'date',
		pagesize: 20,
		page: 1
	},
	
	/**
	 * All search functions return a jQuery Deferred object for easy callback chaining.  
	 * The result of the API call is converted from XML to a user-friendly javascript object(s)
	 * 
	 */
	
	/**
	 * get details for a single asset.  Returns a jQuery deferred object for easy callback chaining.
	 * see http://api.jquery.com/category/deferred-object/ for more information.
	 *
	 * @param {guid} assetId id of asset to load (required)
	 * @param {guid=} applicationId id of player to use (optional)
	 * @return {jQuery Deferred} a jQuery ajax deferred object. 
	 *
	 * Example usage:
			vboss.search.asset('3dc5b319faee47f59e376001e2d0dd53').done(function(asset) {
				alert('video title is: ' + asset.title);
			}).fail(function(error) {
				alert('error loading asset - ' + error.message);
			});
	 * 
	 * 		complete callback (.done()) is called with:
	 *			@param {vboss.asset} asset response from server as asset object
	 *
	 *		error callback (.fail()) is called with:
	 *			@param {jQuery ajax object} xhr ajax object for request
	 *			@param {status} status
	 *			@param {statusText} the failure message
	 */
	asset: function(assetId, applicationId) {
		var url = vboss.search.baseUri + assetId + '/';
		if(applicationId) url += 'applications/' + applicationId + '/';
		
		return vboss.search.ajax(url);
	},
	
	/**
	 * get list of videos based on optional text-based query
	 * 
	 * @param {guid} applicationId Account-specific API Application ID.  (required)
	 * @param {object=} options additional search parameters (optional)
	 * @return {jQuery Deferred} a jQuery ajax deferred object. 
	 *
	 * Example usage:
			// get all videos in account, iterate through multiple pages
			var assetList = [];
			function getAllPages(assets) {
				// add assets to collected asset list 
				assetList = assetList.concat(assets);
				
				// if additional page of results keep collecting pages
				if(assets.next) {
					assets.next()
						.done(getAllPages)
						.fail(handleFailure);
				} else {
					handleResult(assetList);
				}
			}
			
			function handleResult(assetList) {
				alert('found ' + assetList.length + ' assets');
				console.log(assetList);
			}
			
			function handleFailure(error) {
				alert('error loading assets - ' + error.message);
			}
			
			vboss.search.query('32ff5f8f-2455-484a-a5d9-2bfdf7450ae7')
				.done(getAllPages)
				.fail(handleFailure);
	 * 
	 * 		complete callback (.done()) is called with:
	 *			@param {array} assets list of assets.  if single asset is found then vboss.asset object is returned
	 *
	 *		error callback (.fail()) is called with:
	 *			@param {jQuery ajax object} xhr ajax object for request
	 *			@param {status} status
	 *			@param {statusText} the failure message
	 */
	query: function(applicationId, options) {
		options = $.extend({}, vboss.search.defaultOptions, options);
		
		var url = vboss.search.baseUri + 'applications/' + applicationId + '/';
		
		return vboss.search.ajax(url, {
			q: options.query,
			f: options.fields,
			o: options.sort,
			z: options.pagesize,
			n: options.page
		}, options.complete, options.error);
		
	},
	
	/**
	 * get list of videos in a collection
	 *
	 * @param {guid} collectionId id of collection
	 * @param {object} options additional query parameters
	 */
	collection: function(collectionId, options) {
		options = $.extend({}, vboss.search.defaultOptions, options);		
		var url = vboss.search.baseUri + collectionId + '/';
		
		return vboss.search.ajax(url, {
			f: options.fields,
			o: options.sort,
			z: options.pagesize,
			n: options.page
		}, options.complete, options.error);
		
	},
	
	/**
	 * get list of related videos.  Uses the video title as a search query for other 
	 * videos in the account with similar text in the title or description.
	 *
	 * @param {guid} collectionId id of collection
	 * @param {object} options additional query parameters
	 *
	 */
	related: function(assetId, options) {
		options = $.extend({}, vboss.search.defaultOptions, options);
		
		var url = vboss.search.baseUri + assetId + '/';
		if(options.applicationId) url += 'applications/' + applicationId + '/';
		url += 'related/';
		
		return vboss.search.ajax(url, {
			f: options.fields,
			o: options.sort,
			z: options.pagesize,
			n: options.page
		}, options.complete, options.error);
		
	}

};