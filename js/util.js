'use strict'

/**
 * @file VBOSS On-Demand Javascript Library
 * @author Luke Selden
 * @version 0.5
 * 
 *
 * DISCLAIMER: The sample code described herein is provided on an "as is" 
 * basis, without warranty of any kind, to the fullest extent permitted by 
 * law. VBrick does not warrant or guarantee the individual success 
 * developers may have in implementing the sample code on their development 
 * platforms or in using their own Web server configurations. 
 * 
 * VBrick does not warrant, guarantee or make any representations regarding 
 * the use, results of use, accuracy, timeliness or completeness of any 
 * data or information relating to the sample code. VBrick disclaims all 
 * warranties, express or implied, and in particular, disclaims all 
 * warranties of merchantability, fitness for a particular purpose, and 
 * warranties related to the code, or any service or software related 
 * thereto. 
 * 
 * VBrick shall not be liable for any direct, indirect or consequential 
 * damages or costs of any type arising out of any action taken by you or 
 * others related to the sample code. 
 */
 

var vboss = vboss || {};
vboss.util = vboss.util || {};

// utility function to get a query parameter value from a URL
vboss.util.queryParam = function(param, url) {
	url || (url = window.self.location.search);
	var regex = new RegExp('\\b' + param + '=([^&]*)'),
		match = (regex.exec(url) || []).pop();		
	return (match === 'true') ? true : (match === 'false') ? false : match;
}

vboss.util.param = function (obj) {
	return $.param(obj);
}

vboss.util.deparam = function (str) {
	var obj = {};
	str.replace(/([^?=&]+)(=([^&#]*))?/g,function($0,$1,$2,$3) {
		obj[$1] = ($3 === 'true') ? true :
						  ($3 === 'false') ? false :
							(''+$3 === 'undefined') ? undefined :
							/^-?\d*\.?\d*$/.test($3) ? parseFloat($3,10) :
							decodeURIComponent($3);
	});
	return obj;
}

/*
 * event handling functions
 */
 
// TODO - move this to vboss.flashplayer.prototype ?
vboss.util.publish = function(eventName) { 
	$(vboss.util).trigger(eventName, Array.prototype.slice.call(arguments, 1));
};

vboss.notify = vboss.notify || function() {
	vboss.util.handleError('vboss.notify is depreciated, please contact vbrick support');
	vboss.util.publish.apply(undefined, arguments); // backwards compatibility
};

vboss.util.on = function(eventName, callback) {
	$(vboss.util).on(eventName, function() { 
		callback.apply(null, Array.prototype.slice.call(arguments, 1));
	});
};

vboss.util.one = function(eventName, callback) {
	$(vboss.util).one(eventName, function() { 
		callback.apply(null, Array.prototype.slice.call(arguments, 1));
	});
};

vboss.util.off = function(eventName, callback) {
	$(vboss.util).off(eventName, callback);
};

/**
 * this function is called if any errors occur in playback.
 * override for custom error handling, or listen for error event via vboss.util.on('error')
 */
vboss.util.handleError = function(message) {
	$(vboss.util).trigger('error', message);
	if((typeof console != 'undefined') && console.warn) console.warn(message);
};

vboss.handleError = vboss.handleError || function() {
	if((typeof console != 'undefined') && console.warn) console.warn('vboss.notify is depreciated, please contact vbrick support');
	vboss.util.handleError.apply(undefined, arguments); // backwards compatibility
}