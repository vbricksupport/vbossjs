"use strict";

/**
 * @file VBOSS On-Demand Javascript Library
 * @author Luke Selden
 * @version 0.5
 * 
 *
 * DISCLAIMER: The sample code described herein is provided on an "as is" 
 * basis, without warranty of any kind, to the fullest extent permitted by 
 * law. VBrick does not warrant or guarantee the individual success 
 * developers may have in implementing the sample code on their development 
 * platforms or in using their own Web server configurations. 
 * 
 * VBrick does not warrant, guarantee or make any representations regarding 
 * the use, results of use, accuracy, timeliness or completeness of any 
 * data or information relating to the sample code. VBrick disclaims all 
 * warranties, express or implied, and in particular, disclaims all 
 * warranties of merchantability, fitness for a particular purpose, and 
 * warranties related to the code, or any service or software related 
 * thereto. 
 * 
 * VBrick shall not be liable for any direct, indirect or consequential 
 * damages or costs of any type arising out of any action taken by you or 
 * others related to the sample code. 
 */
var vboss = vboss || {};

vboss.util = vboss.util || {};

// utility function to get a query parameter value from a URL
vboss.util.queryParam = function(param, url) {
  url || (url = window.self.location.search);
  var regex = new RegExp("\\b" + param + "=([^&]*)"), match = (regex.exec(url) || []).pop();
  return "true" === match ? true : "false" === match ? false : match;
};

vboss.util.param = function(obj) {
  return $.param(obj);
};

vboss.util.deparam = function(str) {
  var obj = {};
  str.replace(/([^?=&]+)(=([^&#]*))?/g, function($0, $1, $2, $3) {
    obj[$1] = "true" === $3 ? true : "false" === $3 ? false : "" + $3 === "undefined" ? void 0 : /^-?\d*\.?\d*$/.test($3) ? parseFloat($3, 10) : decodeURIComponent($3);
  });
  return obj;
};

/*
 * event handling functions
 */
vboss.util.publish = function(eventName) {
  $(vboss.util).trigger(eventName, Array.prototype.slice.call(arguments, 1));
};

vboss.notify = vboss.notify || function() {
  vboss.util.handleError("vboss.notify is depreciated, please contact vbrick support");
  vboss.util.publish.apply(void 0, arguments);
};

vboss.util.on = function(eventName, callback) {
  $(vboss.util).on(eventName, function() {
    callback.apply(null, Array.prototype.slice.call(arguments, 1));
  });
};

vboss.util.one = function(eventName, callback) {
  $(vboss.util).one(eventName, function() {
    callback.apply(null, Array.prototype.slice.call(arguments, 1));
  });
};

vboss.util.off = function(eventName, callback) {
  $(vboss.util).off(eventName, callback);
};

/**
 * this function is called if any errors occur in playback.
 * override for custom error handling, or listen for error event via vboss.util.on('error')
 */
vboss.util.handleError = function(message) {
  $(vboss.util).trigger("error", message);
  "undefined" != typeof console && console.warn && console.warn(message);
};

vboss.handleError = vboss.handleError || function() {
  "undefined" != typeof console && console.warn && console.warn("vboss.notify is depreciated, please contact vbrick support");
  vboss.util.handleError.apply(void 0, arguments);
};

// Internet Explorer polyfill for isostring
Date.prototype.toISOString || !function() {
  function pad(num) {
    var r = num.toFixed();
    1 === r.length && (r = "0" + r);
    return r;
  }
  Date.prototype.toISOString = function() {
    return [ this.getUTCFullYear(), "-", pad(this.getUTCMonth() + 1), "-", pad(this.getUTCDate()), "T", pad(this.getUTCHours()), ":", pad(this.getUTCMinutes()), ":", pad(this.getUTCSeconds()), ".", (this.getUTCMilliseconds() / 1e3).toFixed(3).toString().slice(2, 5), "Z" ].join("");
  };
}();

// use XDomainRequest for IE, to allow CORS
// adapted from 
// https://github.com/jaubourg/ajaxHooks/blob/master/src/xdr.js
window.XDomainRequest && $.ajaxTransport(function(s) {
  if (s.crossDomain && s.async) {
    if (s.timeout) {
      s.xdrTimeout = s.timeout;
      delete s.timeout;
    }
    var xdr;
    return {
      send: function(_, complete) {
        function callback(status, statusText, responses, responseHeaders) {
          xdr.onload = xdr.onerror = xdr.ontimeout = $.noop;
          xdr = void 0;
          complete(status, statusText, responses, responseHeaders);
        }
        xdr = new XDomainRequest();
        xdr.onload = function() {
          var responses = {
            text: xdr.responseText
          };
          // include parsed xml if valid
          try {
            responses.xml = $.parseXML(xdr.responseText);
          } catch (e) {}
          callback(200, "OK", responses, "Content-Type: " + xdr.contentType);
        };
        xdr.onerror = function() {
          callback(404, "Not Found");
        };
        xdr.onprogress = $.noop;
        xdr.ontimeout = function() {
          callback(0, "timeout");
        };
        xdr.timeout = s.xdrTimeout || Number.MAX_VALUE;
        xdr.open(s.type, s.url);
        setTimeout(function() {
          xdr.send(s.hasContent && s.data || null);
        }, 0);
      },
      abort: function() {
        if (xdr) {
          xdr.onerror = $.noop;
          xdr.abort();
        }
      }
    };
  }
});

/************************************************/
/**
 * Asset - wrapper class for asset entries from REST Search Services API
 */
vboss.asset = function(data) {
  var _this = this;
  // ensure new instance
  if (!this instanceof vboss.asset) {
    return new vboss.asset(data);
  }
  // create asset from xml if provided (or array of assets if xml includes multiple entries
  if (data && $.isXMLDoc(data)) {
    return this.populateFromXml(data);
  }
  // if xml not included return empty asset
  this.id = null;
  this.title = "";
  this.description = "";
  this.keywords = "";
  this.categories = {};
  this.link = "";
  this.date = null;
  this.preview = "";
  this.player = {
    applicationId: null,
    width: null,
    height: null
  };
  this.thumbnails = null;
  this.metrics = {
    duration: 0,
    state: "",
    plays: 0,
    shares: 0,
    votes: 0,
    rating: 0
  };
  return this;
};

/**
 * converts api results to javascript object
 * if xml includes multiple asset entries then returns an array of objects
 */
vboss.asset.prototype.populateFromXml = function(xml) {
  // use node if given <item>, otherwise find all item nodes
  var $node = $(xml).is("item") ? $(xml) : $("item", xml);
  if (0 == $node.length) {
    var itemTotal = _node("itemTotal", "fzat").text();
    if ("" == itemTotal || "0" === itemTotal) {
      return new vboss.asset();
    }
  } else {
    if ($node.length > 1) {
      return $.map($node, function(item) {
        return new vboss.asset(item);
      });
    }
  }
  // private helper function - browser-agnostic search for namespaced xml nodes
  function _node(key, namespace) {
    return namespace ? $([ namespace, "\\:", key, ", ", key ].join(""), $node[0]) : $(key, $node[0]);
  }
  var item = new vboss.asset();
  // get Metadata
  item.id = _node("guid").text();
  item.title = _node("title").text();
  item.description = _node("description").text();
  item.keywords = _node("keywords", "media").text();
  item.link = _node("link").text();
  item.date = new Date(_node("pubDate").text());
  // include any category values
  item.categories = {};
  _node("category").each(function(i, category) {
    var key = ($(category).attr("domain") || "").replace(/(.*):/, "");
    item.categories[key] = $(category).text();
  });
  // get Player Info
  var playerNode = _node("player", "media");
  item.player.applicationId = playerNode.attr("url").replace(/.*\/(.*)\.swf.*/, "$1");
  item.player.width = playerNode.attr("width");
  item.player.height = playerNode.attr("height");
  // preview and thumbnails
  var thumbnailNodes = _node("thumbnail", "media");
  item.thumbnails = $.map(thumbnailNodes, function(thumbnail, i) {
    var $thumb = $(thumbnail);
    // check if default preview
    if ("true" == $thumb.attr("fzat:isDefault")) {
      item.preview = $thumb.attr("url");
      return null;
    }
    return {
      url: $thumb.attr("url"),
      time: $thumb.attr("time")
    };
  });
  // metrics
  item.metrics = {};
  // video information
  var contentNode = _node("content", "media");
  item.metrics.duration = parseInt(contentNode.attr("duration")) || 0;
  item.metrics.state = contentNode.attr("fzat:state");
  item.metrics.approved = contentNode.attr("fzat:approved");
  // metrics information
  var metricsNode = _node("metrics", "fzat");
  item.metrics.plays = parseInt(metricsNode.attr("plays")) || 0;
  item.metrics.shares = parseInt(metricsNode.attr("shares")) || 0;
  item.metrics.votes = parseInt(metricsNode.attr("votes")) || 0;
  item.metrics.ratings = parseInt(metricsNode.attr("ratings")) || 0;
  // 0.5 for backwards compatibility.  Previously always showed 0
  item.metrics.rating = item.metrics.ratings;
  return item;
};

/**
 * embed video in the specified div using the SmartTag
 * 
 * @param {string} containerId video content will be loaded into div with specified id
 * @param {object=} options hash of additional embed settings, which may be: (optional)
 *		@param {string=} width css string for width of player (i.e. 640px). default is fill container
 *		@param {string=} height css string for height
 *		@param {boolean=} autoplay start video on load (flash-only)
 *		@param {boolean=} automute start video silent (flash-only)
 *		@param {string=} bgcolor background color of player (flash-only)
 *
 */
vboss.asset.prototype.embed = function(containerId, options) {
  options = options || {};
  var protocol = /https/.test(document.location.protocol) ? "https:" : "http:", smartTagUrl = protocol + "//services.fliqz.com/smart/20100401/applications/" + this.player.applicationId + "/assets/" + this.id + "/containers/" + containerId + "/smarttag.js";
  return $.ajax({
    url: smartTagUrl,
    data: {
      width: options.width || "100%",
      height: options.height || "100%",
      autoPlayback: options.autoplay || false,
      audioMute: options.automute || false,
      bgcolor: options.bgcolor || "#000000"
    },
    dataType: "script",
    cache: "true"
  });
};

/**
 * vboss.search is a wrapper around the VBOSS On-Demand REST Search Services API
 */
vboss.search = {
  /**
	 * private function to handle ajax requests
	 *
	 * @param {string} url The url to load
	 * @param {object=} data additional query parameters (optional)
	 * @param {function=} completeCallback function called when done (optional)
	 * @param {function=} errorCallback function called on error (optional)
	 */
  ajax: function(url, data, completeCallback, errorCallback) {
    var request = $.ajax({
      url: url,
      data: data,
      type: "GET",
      cache: true,
      crossDomain: true,
      dataType: "xml",
      converters: {
        "text xml": $.parseXML
      }
    }).pipe(function(xml) {
      var itemTotal = $("itemTotal, fzat\\:itemTotal", xml).text(), results;
      if (0 == itemTotal) {
        vboss.util.handleError("search: no results");
        results = [];
        results.itemTotal = 0;
        results.pages = 0;
        results.page = 0;
        return results;
      }
      results = new vboss.asset(xml);
      // if a collection of assets then augment array with information about collection, and include pagination functions
      if (itemTotal > 1) {
        results.itemTotal = itemTotal;
        results.pages = $("pages, fzat\\:pages", xml).text();
        results.page = ($("[rel=self]", xml).attr("href") || "").match(/n=(\d+)/) ? RegExp.$1 : "1";
        var prev = $('[rel="previous"]', xml).attr("href"), next = $('[rel="next"]', xml).attr("href"), first = $('[rel="first"]', xml).attr("href"), last = $('[rel="last"]', xml).attr("href");
        prev && (results.prev = function(callback, errorCallback) {
          return vboss.search.ajax(prev, null, callback, errorCallback);
        });
        next && (results.next = function(callback, errorCallback) {
          return vboss.search.ajax(next, null, callback, errorCallback);
        });
        first && (results.first = function(callback, errorCallback) {
          return vboss.search.ajax(first, null, callback, errorCallback);
        });
        last && (results.last = function(callback, errorCallback) {
          return vboss.search.ajax(last, null, callback, errorCallback);
        });
      }
      return results;
    }, function(xhr) {
      // on error provide useful message
      var statusCode = xhr.status, message = "";
      switch (statusCode) {
       case 403:
        message = "The Account is suspended or Application does not allow API access.";
        break;

       case 404:
        message = "The Application or Asset is unknown, or the URI used to access the service is incorrect.";
        break;

       case 500:
        message = "An unrecoverable server side error has occurred.";
        break;

       default:
        message = xhr.statusText;
      }
      return {
        xhr: xhr,
        status: statusCode,
        message: message
      };
    });
    $.isFunction(completeCallback) && request.done(completeCallback);
    $.isFunction(errorCallback) && request.fail(errorCallback);
    return request;
  },
  /* url for REST Service */
  baseUri: (/https/.test(window.self.location.protocol) ? "https:" : "http:") + "//services.fliqz.com/assets/20090819/AssetSearch.svc/",
  /**
	 * default search options - see documentation for available values
	 */
  defaultOptions: {
    fields: "all",
    sort: "date",
    pagesize: 20,
    page: 1
  },
  /**
	 * All search functions return a jQuery Deferred object for easy callback chaining.  
	 * The result of the API call is converted from XML to a user-friendly javascript object(s)
	 * 
	 */
  /**
	 * get details for a single asset.  Returns a jQuery deferred object for easy callback chaining.
	 * see http://api.jquery.com/category/deferred-object/ for more information.
	 *
	 * @param {guid} assetId id of asset to load (required)
	 * @param {guid=} applicationId id of player to use (optional)
	 * @return {jQuery Deferred} a jQuery ajax deferred object. 
	 *
	 * Example usage:
			vboss.search.asset('3dc5b319faee47f59e376001e2d0dd53').done(function(asset) {
				alert('video title is: ' + asset.title);
			}).fail(function(error) {
				alert('error loading asset - ' + error.message);
			});
	 * 
	 * 		complete callback (.done()) is called with:
	 *			@param {vboss.asset} asset response from server as asset object
	 *
	 *		error callback (.fail()) is called with:
	 *			@param {jQuery ajax object} xhr ajax object for request
	 *			@param {status} status
	 *			@param {statusText} the failure message
	 */
  asset: function(assetId, applicationId) {
    var url = vboss.search.baseUri + assetId + "/";
    applicationId && (url += "applications/" + applicationId + "/");
    return vboss.search.ajax(url);
  },
  /**
	 * get list of videos based on optional text-based query
	 * 
	 * @param {guid} applicationId Account-specific API Application ID.  (required)
	 * @param {object=} options additional search parameters (optional)
	 * @return {jQuery Deferred} a jQuery ajax deferred object. 
	 *
	 * Example usage:
			// get all videos in account, iterate through multiple pages
			var assetList = [];
			function getAllPages(assets) {
				// add assets to collected asset list 
				assetList = assetList.concat(assets);
				
				// if additional page of results keep collecting pages
				if(assets.next) {
					assets.next()
						.done(getAllPages)
						.fail(handleFailure);
				} else {
					handleResult(assetList);
				}
			}
			
			function handleResult(assetList) {
				alert('found ' + assetList.length + ' assets');
				console.log(assetList);
			}
			
			function handleFailure(error) {
				alert('error loading assets - ' + error.message);
			}
			
			vboss.search.query('32ff5f8f-2455-484a-a5d9-2bfdf7450ae7')
				.done(getAllPages)
				.fail(handleFailure);
	 * 
	 * 		complete callback (.done()) is called with:
	 *			@param {array} assets list of assets.  if single asset is found then vboss.asset object is returned
	 *
	 *		error callback (.fail()) is called with:
	 *			@param {jQuery ajax object} xhr ajax object for request
	 *			@param {status} status
	 *			@param {statusText} the failure message
	 */
  query: function(applicationId, options) {
    options = $.extend({}, vboss.search.defaultOptions, options);
    var url = vboss.search.baseUri + "applications/" + applicationId + "/";
    return vboss.search.ajax(url, {
      q: options.query,
      f: options.fields,
      o: options.sort,
      z: options.pagesize,
      n: options.page
    }, options.complete, options.error);
  },
  /**
	 * get list of videos in a collection
	 *
	 * @param {guid} collectionId id of collection
	 * @param {object} options additional query parameters
	 */
  collection: function(collectionId, options) {
    options = $.extend({}, vboss.search.defaultOptions, options);
    var url = vboss.search.baseUri + collectionId + "/";
    return vboss.search.ajax(url, {
      f: options.fields,
      o: options.sort,
      z: options.pagesize,
      n: options.page
    }, options.complete, options.error);
  },
  /**
	 * get list of related videos.  Uses the video title as a search query for other 
	 * videos in the account with similar text in the title or description.
	 *
	 * @param {guid} collectionId id of collection
	 * @param {object} options additional query parameters
	 *
	 */
  related: function(assetId, options) {
    options = $.extend({}, vboss.search.defaultOptions, options);
    var url = vboss.search.baseUri + assetId + "/";
    options.applicationId && (url += "applications/" + applicationId + "/");
    url += "related/";
    return vboss.search.ajax(url, {
      f: options.fields,
      o: options.sort,
      z: options.pagesize,
      n: options.page
    }, options.complete, options.error);
  }
};

/**
 * VBOSS On-Demand Metrics Service
 *
 * metrics constructor
 * this class allows you to use a native HTML5 player with VBOSS On-Demand videos.
 * it allows you to initiate a play event and retrieve the URL of a video asset MP4,
 * and allows logging of playback events (pause, seek, etc) with the VBOSS Metrics Service.
 * 
 * @param {guid} applicationId REQUIRED - the REST Search Services ApplicationID / PlayerID for 
 * 		your account.  The asset version of the video (HD, SD, mobile/low-bitrate) will 
 * 		correspond to the asset version for the given PlayerID
 */
vboss.metrics = function(applicationId) {
  if (!(this instanceof vboss.metrics)) {
    return new vboss.metrics(applicationId);
  }
  this.applicationId = applicationId;
  this.sessionId = null;
  this.assetId = null;
  this.assetUrl = null;
  this.el = null;
  // for html5 support
  this._startTime = new Date();
  // time metrics session started
  return this;
};

/**
 * constants
 * baseUri: - url for POST requests
 * metricsUpdateInterval: rate at which 'continue' events are sent to server.
 */
vboss.metrics.prototype.ERROR_MESSAGES = {
  400: "The input parameters are invalid or malformed.",
  403: "The Account owning the Application has been suspended.",
  404: "The ApplicationID, SessionID or AssetID is unknown.",
  500: "An unrecoverable server side error has occurred."
};

vboss.metrics.prototype.baseUri = (/https/.test(window.self.location.protocol) ? "https:" : "http:") + "//services.fliqz.com/metrics/20090201/MetricsService.svc/";

vboss.metrics.prototype.metricsUpdateInterval = 10;

/**
 * internal function - make ajax calls
 */
vboss.metrics.prototype.ajax = function(url, params, callback, errorCallback) {
  var ERROR_MESSAGES = this.ERROR_MESSAGES, request = $.ajax({
    url: url + params,
    type: "POST",
    cache: false,
    crossDomain: true,
    dataType: "xml",
    converters: {
      "text xml": $.parseXML
    }
  }).pipe(null, function(xhr) {
    // provide useful error message on failure
    var statusCode = xhr.status, message = ERROR_MESSAGES[statusCode] || xhr.statusText;
    return {
      xhr: xhr,
      status: statusCode,
      message: message
    };
  });
  $.isFunction(callback) && request.done(callback);
  $.isFunction(errorCallback) && request.fail(errorCallback);
  return request;
};

/**
 * createSession - request a new metrics session from VBrick.  returns sessionId to callback
 * optionally include callbacks to be called when load is complete or an error occurs
 */
vboss.metrics.prototype.createSession = function(callback, errorCallback) {
  var _this = this, url = [ this.baseUri, "applications/", this.applicationId, "/sessions/" ].join(""), params = [ "?offset=", this._offsetTime(), // time from player load
  "&ct=", new Date().toISOString(), // current time in ISO format
  "&uri=", document.location.href ].join(""), request = this.ajax(url, params).pipe(function(response) {
    _this.sessionId = $("fqzsm\\:sid, sid", response).text();
    // xml selector (fix for differing browser compatibility with namespaces
    _this.serverTime = $("fqzsm\\:st, st", response).text();
    // returns server time -- not needed for any subsequent requests
    return _this.sessionId;
  });
  $.isFunction(callback) && request.done(callback);
  $.isFunction(errorCallback) && request.fail(errorCallback);
  return request;
};

/**
 * load - log a play event and retrieve media asset URL
 * @param {guid} assetId REQUIRED - specify the asset to play
 * @param {function=} callback - function called on successful load.  returns url of asset MP4
 * @param {function=} error - function called on load error. returns server response
 * @returns {jQuery Deferred} deferred object for use with .done and .fail
 */
vboss.metrics.prototype.load = function(assetId, callback, errorCallback) {
  var _this = this;
  this.assetId = assetId;
  // construct the URL with REST parameters
  var url = [ this.baseUri, "applications/", this.applicationId, "/sessions/", this.sessionId, "/assets/", this.assetId, "/events/", "load/" ].join(""), params = [ "?offset=", this._offsetTime() ].join(""), request = this.ajax(url, params).pipe(function(response) {
    _this.assetUrl = $("fqzsm\\:u, u", response).text();
    // retrieve MP4 url
    return _this.assetUrl;
  });
  $.isFunction(callback) && request.done(callback);
  $.isFunction(errorCallback) && request.fail(errorCallback);
  return request;
};

/**
 * registerEvent -- send metrics events (play, pause, seek, etc) to server
 * pulls in current time / video time using private functions
 * @param eventType REQUIRED - see addEventHandlers for tracked events
 *
 */
vboss.metrics.prototype.registerEvent = function(eventType) {
  // construct the URL with REST parameters
  var url = [ this.baseUri, //'applications/', this.applicationId,
  "sessions/", this.sessionId, "/assets/", this.assetId, "/events/", eventType, "/" ].join(""), params = [ "?offset=", this._offsetTime(), "&assetTime=", this._currentTime() ].join("");
  // make ajax call to server
  return this.ajax(url, params);
};

// private function - get time from initial metrics load
vboss.metrics.prototype._offsetTime = function() {
  var time = (new Date() - this._startTime) / 1e3;
  return "PT" + Math.round(time) + "S";
};

// private function - get current video playback time
vboss.metrics.prototype._currentTime = function() {
  return this.el ? "PT" + Math.round(this.el.currentTime) + "S" : null;
};

/**
 * addEventHandlers -- register metrics event on HTML5 audio/video element
 * this will register all metrics events with the supplied element
 *
 * @param {jQuery selector} element selector for element to bind.  may be selector or DOM element
 */
vboss.metrics.prototype.addEventHandlers = function(element) {
  var _this = this, lastTimeUpdate = 0, seeking = false, $el = $(element);
  // store element reference
  _this.el = $el[0];
  if (!_this.el || void 0 == _this.el.currentTime) {
    throw "invalid element or no element defined";
  }
  // on these HTML5 events a metrics event will be sent to server
  $el.on("play", function() {
    _this.registerEvent("play");
  });
  $el.on("pause", function() {
    true !== _this.el.ended && // ignore pause event at end of video
    _this.registerEvent("pause");
  });
  $el.on("resume", function() {
    _this.registerEvent("play");
  });
  $el.on("ended", function() {
    _this.registerEvent("complete");
  });
  $el.on("seeking", function() {
    if (seeking) {
      return false;
    }
    // avoid duplicate events
    seeking = true;
    _this.registerEvent("dstart");
  });
  $el.on("seeked", function() {
    seeking = false;
    _this.registerEvent("dend");
  });
  $el.on("timeupdate", function() {
    if (_this._seeking) {
      return false;
    }
    // don't send updates during seek
    var time = _this.el.currentTime;
    // send an update event only if interval between updates has passed
    if (lastTimeUpdate + _this.metricsUpdateInterval > time) {
      return false;
    }
    lastTimeUpdate = time;
    _this.registerEvent("continue");
  });
  $el = null;
  return _this;
};

/**
 * vboss.player allows playback of assets using HTML5 or flash, depending on browser capabilites
 *
 * @param {guid} applicationId the ID of the player to use.  If you do not have this value contact VBrick Support
 * @param {string} containerId the ID of the div in which to place player content.  The node with this ID will be REPLACED by the video player.
 * @param {object} options additional configuration options, which may be:
 *		@param {boolean=} preferHTML5 If set to true then use HTML5 player on browsers that support it, even if they support flash as well.
 *		@param {string=} width css string for width of player (i.e. 640px). default is fill container
 *		@param {string=} height css string for height
 *		@param {boolean=} autoplay start video on load
 *		@param {boolean=} automute start video silent
 *		@param {string=} bgcolor background color of player
 *		@param {string=} wmode (flash only) wmode to use (transparent, opaque or window)
 *		@param {mixed=} theme (html5 only) set the encoding theme for account.  See versionSelect class for options
 *
 */
vboss.player = function(applicationId, containerId, _options) {
  var options = $.extend({}, this.defaultOptions, _options), support = this.detectCapabilities(), playerClass;
  // use flash if able
  if (support.flash) {
    // ...except if html5 is preferred
    playerClass = options.preferRTMP && support.rtmp ? vboss.player.rtmp : options.preferHTML5 && support.mp4 ? vboss.player.html5 : vboss.player.flash;
  } else {
    if (support.html5) {
      options.flashFallback = false;
      // flash isn't possible
      playerClass = vboss.player.html5;
    } else {
      options.smartTagFallback && (playerClass = vboss.player.smartTag);
    }
  }
  // return selected class if found
  if (playerClass) {
    return new playerClass(applicationId, containerId, options);
  }
  // fallback is to just display a preview and direct link to MP4
  this.applicationId = applicationId;
  this.containerId = containerId;
  this.options = $.extend({}, this.defaultOptions, options);
  vboss.util.handleError("browser does not support playback. falling back to direct link");
  return this;
};

vboss.player.prototype.defaultOptions = {
  preferHTML5: false,
  smartTagFallback: true,
  flashFallback: true,
  // flash fallback for HTML5 player
  width: "100%",
  height: "100%",
  autoplay: false,
  automute: false,
  bgcolor: "#000000",
  wmode: "transparent",
  // change according to account settings - see vboss.util.versionSelect
  theme: "standard",
  // use default version unless specified -- avoids delays when theme isn't set
  version: "default"
};

/**
 * load given asset
 *
 * @param {guid} assetId id of asset to play
 * @param {object=} options
 */
vboss.player.prototype.load = function(assetId, options) {
  options = $.extend({}, this.defaultOptions, options);
  this.assetId = assetId;
  $("#" + this.containerId).html([ '<div style="position:relative;">', '<a href="http://cdn9.fliqz.com/', assetId, '.mp4">', '<img src="http://previews.fliqz.com/', assetId, '.jpg" alt="Click to play"', ' style="width:', options.width, ";height:", options.height, '" />', '<img style="position:absolute;left:50%;top:50%;z-index:1000;border:0px none;margin:-25px 0 0 -145px;max-width:100%;"', ' src="http://images.fliqz.com/b7bc4f9192b34cf1a9566305fd94a4ae.png" alt="Play" />', "</a>", "</div>" ].join(""));
  // return a promise for compatibility with other player types
  var promise = new $.Deferred();
  promise.resolve();
  return promise;
};

/**
 * detect if browser supports html5 and flash
 */
vboss.player.prototype.detectCapabilities = function() {
  var support = {};
  /* use SWFObject to detect if flash is available */
  if ("undefined" != typeof swfobject && swfobject) {
    support.flash = swfobject.hasFlashPlayerVersion("9.0.115");
    support.rtmp = swfobject.hasFlashPlayerVersion("10.0.000");
  } else {
    support.flash = false;
    if ("ActiveXObject" in window) {
      try {
        support.flash = !!new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
      } catch (e) {
        support.flash = false;
      }
    } else {
      support.flash = !!navigator.mimeTypes["application/x-shockwave-flash"];
    }
    // can't detect version using this method, so just assume high enough
    support.rtmp = support.flash;
  }
  // html5 support
  var node = document.createElement("video");
  support.html5 = "undefined" != typeof node.canPlayType;
  if (!support.html5) {
    return support;
  }
  // mp4 support (check if firefox)
  support.mp4 = "" != node.canPlayType("video/mp4").replace(/no/, "");
  if (!support.mp4) {
    return support;
  }
  // if andoroid pre-2.3 then can't handle html5 properly
  var ua = window.navigator.userAgent.toLowerCase();
  support.html5controls = !ua.match("android 2.[12]");
  return support;
};

function testFlash() {}

/**
 * versionSelect is used for managing the multiple versions of each asset
 * once uploaded to the VBOSS services a video is encoded to multiple versions,
 * each at different bitrates/resolutions/settings to target a particular resolution
 * or device compatibility.  This class is used to load a specified version.
 * If an error occurs while loading a version (set via versionSelect.failCurrent()) then it is skipped
 * and the current version is set to the next available.
 *
 * Usage:
 * // create instance
 * versionSelect = new vboss.versionSelect('standard');
 * 
 * // load asset via ID, and default URL (if available).  
 * // initializes to use highest quality version available
 * versionSelect.load(assetId, defaultUrl);
 * url = versionSelect.currentSrc;
 *
 * // set to specified quality level, retrieve current URL
 * url = versionSelect.setByName('mobile').currentSrc;
 * // or set by preferred resolution
 * url = versionSelect.setByResolution(360).currentSrc;
 *  
 * NOTE: each VBOSS account is configured with an encoding 'theme' that specifies
 * what versions are created for each asset.  The most common one is our 'standard' encoding.
 * If your account is configured to a different setting then initialize the class with a different theme.
 * Check with VBrick Support for your account's settings.
 *
 */
!function(vboss, $) {
  var BASE_URL = "http://cdn9.fliqz.com/", CLASSIFICATIONS = {
    screencap: {
      name: "screencap",
      id: "afc286b7786a43f68aeacdff286b0722",
      type: "video/mp4; codecs='avc1.64401F, mp4a.40.5'",
      resolution: 1080,
      bitrate: 400
    },
    hd: {
      // hd 720p
      name: "hd",
      id: "827283d4f0a4475f9ca40b94417ced0e",
      type: "video/mp4; codecs='avc1.64401F, mp4a.40.5'",
      resolution: 720,
      bitrate: 2100
    },
    hq: {
      // 540p appletv/iPad compatible
      name: "hq",
      id: "f3947812b8f8401eb6097d4ac9ecf98b",
      type: "video/mp4; codecs='avc1.4D401F, mp4a.40.5'",
      resolution: 540,
      bitrate: 1500
    },
    sd: {
      // 360p standard
      name: "sd",
      id: "e8763a88ea7d443794ac94c4379f2df7",
      type: "video/mp4; codecs='avc1.64001F, mp4a.40.5'",
      resolution: 360,
      bitrate: 800
    },
    mobile: {
      // 360p mobile medium quality
      name: "mobile",
      id: "0f849b15cfb6401ba59ba38584249412",
      type: "video/mp4; codecs='avc1.42E016, mp4a.40.2'",
      resolution: 320,
      bitrate: 700
    },
    mobilelow: {
      // 240p low bitrate mobile
      name: "mobilelow",
      id: "2872eaf6dc6643fb8c80e66038b64084",
      type: "video/mp4; codecs='avc1.42E016, mp4a.40.2'",
      resolution: 240,
      bitrate: 500
    },
    lowest: {
      // 180p lowest possible
      name: "lowest",
      id: "773c95496a384ef6a6155d61697c9d8e",
      type: "video/mp4",
      resolution: 180,
      bitrate: 300
    }
  }, THEMES = {
    standard: [ "sd", "mobile", "mobilelow" ],
    balanced: [ "hd", "sd", "mobilelow" ],
    screen: [ "screencap", "sd", "mobilelow" ],
    hd: [ "hd", "hq", "sd" ],
    vems: [ "hd", "sd", "mobile" ],
    settop: [ "hq", "mobile", "mobilelow" ],
    mobile: [ "mobile", "mobilelow", "lowest" ]
  }, versionSelect = function(theme) {
    theme = $.isArray(theme) ? theme : THEMES[theme] || THEMES.standard;
    var videoElement = document.createElement("video"), // firefox reports "no" if codec is specified, so check if it can play MP4 but not lowest quality baseline
    isBrowserMisreportingCodecSupport = videoElement.canPlayType("video/mp4") && !videoElement.canPlayType("video/mp4; codecs='avc1.42E016, mp4a.40.2'");
    this.assetId = null;
    this.current = null;
    this.currentSrc = null;
    this.currentName = null;
    // include versions in theme if browser supports type
    this.versions = [];
    for (var i = 0, name; name = theme[i]; i++) {
      var version = $.extend({
        url: null,
        error: false
      }, CLASSIFICATIONS[name]), canPlay = videoElement.canPlayType(version.type);
      (isBrowserMisreportingCodecSupport || canPlay.replace(/no/, "")) && this.versions.push(version);
    }
    videoElement = null;
    // include default version
    this["default"] = {
      name: "default",
      id: "default",
      type: "video/mp4",
      resolution: 0,
      bitrate: 0,
      error: false,
      url: null
    };
    // change to use default first
    //this.versions.push(this['default']);
    this.versions.unshift(this["default"]);
    return this;
  };
  /**** functions ***/
  // initialize with asset information		
  versionSelect.prototype.load = function(assetId, defaultUrl) {
    this.assetId = assetId.replace(/-/g, "");
    if (defaultUrl && defaultUrl.match(/\.flv/)) {
      vboss.util.publish("flverror");
      throw new Error("FLV playback via HTML5 not supported");
    }
    // set url for each version
    for (var i = 0, version; version = this.versions[i]; i++) {
      version.error = null;
      // reset all errors
      version.url = this.generateVersionUrl(version, assetId, defaultUrl);
    }
    this.update();
    return this;
  };
  versionSelect.prototype.generateVersionUrl = function(version, assetId, defaultUrl) {
    var baseUrl = BASE_URL + assetId + ".mp4";
    return "default" === version.name ? defaultUrl || baseUrl : baseUrl + "?acl=" + version.id;
  };
  // set current version to one specified by name, or first available
  versionSelect.prototype.update = function(name) {
    var version = name ? this.getVersionByName(name) : this.getFirstAvailableVersion();
    if (void 0 == version) {
      // add if version not in theme
      if (!CLASSIFICATIONS[name]) {
        this.current = null;
        this.currentSrc = null;
        this.currentName = null;
        throw new Error("no available version found");
      }
      version = $.extend({}, CLASSIFICATIONS[name]);
      version.error = null;
      version.url = this.generateVersionUrl(version, assetId);
      this.versions.push(version);
    }
    this.current = version;
    this.currentSrc = version.url;
    this.currentName = version.name;
    return this;
  };
  // set current version by name
  versionSelect.prototype.setByName = function(name) {
    this.update(name);
    return this;
  };
  // set current version by preferred resolution.  if no exact match found returns closest option
  versionSelect.prototype.setByResolution = function(resolution) {
    resolution = parseInt(resolution);
    var closest = this.versions[0], diff = Math.abs(closest.resolution - resolution), match = this.versions.filter(function(version) {
      if (version.resolution == resolution) {
        return true;
      }
      if (Math.abs(version.resolution - resolution) < diff) {
        diff = Math.abs(version.resolution - resolution);
        closest = version;
      }
      return false;
    });
    this.update(match.length ? match[0].name : closest.name);
    return this;
  };
  // return version specified by name, or undefined if not found
  versionSelect.prototype.getVersionByName = function(name) {
    return this.versions.filter(function(version) {
      return version.name == name;
    })[0];
  };
  // return first version that hasn't failed
  versionSelect.prototype.getFirstAvailableVersion = function() {
    return this.versions.filter(function(version) {
      return !version.error;
    })[0];
  };
  // called on error with current src, load alternate
  versionSelect.prototype.failCurrent = function() {
    this.current && (this.current.error = true);
    this.update();
  };
  vboss.player.versionSelect = versionSelect;
  return vboss;
}(vboss, $);

/**
 * wrapper around vboss smartTag embed
 */
vboss.player.smartTag = function(applicationId, containerId, options) {
  var _this = this;
  this.applicationId = applicationId.replace(/-/g, "");
  this.containerId = containerId;
  this.assetId = null;
  this.options = $.extend({}, this.defaultOptions, options);
  return this;
};

$.extend(vboss.player.smartTag.prototype, vboss.player.prototype, {
  load: function(assetId, options) {
    options = $.extend({}, this.options, options);
    // load smart tag
    var protocol = /https/.test(document.location.protocol) ? "https:" : "http:", smartTagUrl = protocol + "//services.fliqz.com/smart/20100401/applications/" + this.applicationId + "/assets/" + assetId + "/containers/" + this.containerId + "/smarttag.js";
    this.assetId = assetId;
    return $.ajax({
      url: smartTagUrl,
      data: {
        width: options.width || "100%",
        height: options.height || "100%",
        autoPlayback: options.autoplay || false,
        audioMute: options.automute || false,
        bgcolor: decodeUriComponent(options.bgcolor || "#000000")
      },
      dataType: "script",
      cache: "true"
    });
  }
});

!function(vboss, $) {
  /**
	 * html5 player
	 */
  vboss.player.html5 = function(applicationId, containerId, options) {
    this.applicationId = applicationId;
    this.containerId = containerId;
    this.assetId = null;
    this.options = $.extend({}, this.defaultOptions, options);
    this.volume = this.options.volume || 1;
    // connect to metrics service and initialize session
    this.loaded = false;
    this.metrics = new vboss.metrics(applicationId);
    this.$el = this._createVideoElement();
    this.metrics.addEventHandlers(this.$el[0]);
    this._addEventHandlers(this.$el);
    // swap existing content with video
    $("#" + this.containerId).replaceWith(this.$el);
    this.sessionRequest = this.metrics.createSession();
    // versionSelect keeps track of available quality options
    this.versionSelect = new vboss.player.versionSelect(this.options.theme);
    // in instances where only a FLV version is available for a video then fallback to flash
    options.flashFallback && // this event is only fired by versionSelect object
    vboss.util.on("flverror", function() {
      var flashPlayer = new vboss.player.flash(applicationId, containerId, this.options);
      flashPlayer.load(this.assetId);
    });
    return this;
  };
  $.extend(vboss.player.html5.prototype, vboss.player.prototype, {
    seekIncrement: 5,
    // keyboard handler seek amount (in seconds)
    /**
		 * load an asset.  
		 * see vboss.player.load for parameters
		 */
    load: function(assetId, options) {
      var self = this, $el = this.$el, readyPromise = $.Deferred(), startTime = 0, autoplay, version;
      options = $.extend({}, this.options, options);
      autoplay = options.autoplay;
      startTime = options.startTime || 0;
      version = options.version;
      this.assetId = assetId;
      this.assetUrl = null;
      $el.attr("poster", "http://previews.fliqz.com/" + this.assetId + ".jpg");
      $el.css({
        width: options.width,
        height: options.height,
        "background-color": decodeURIComponent(options.bgcolor)
      });
      options.automute && ($el.volume = 0);
      this.sessionRequest.then(function() {
        // log metrics load and get default asseturl
        self.metrics.load(self.assetId).done(function(assetUrl) {
          self.assetUrl = assetUrl;
        }).fail(function(error) {
          self.assetUrl = null;
          readyPromise.reject(error);
        }).then($.proxy(function() {
          this.loaded = true;
          try {
            // update the urls for each quality version
            self.versionSelect.load(self.assetId, self.assetUrl);
          } catch (e) {
            vboss.util.handleError(e.message);
            readyPromise.reject();
            // error, FLV
            return;
          }
          var canPlayPromise = self._updateSource($el, startTime, version);
          autoplay ? canPlayPromise.done(function() {
            self.play();
            readyPromise.resolve();
          }).fail(function() {
            readyPromise.reject();
          }) : readyPromise.resolve();
        }, self));
      });
      return readyPromise;
    },
    /**
		 * returns active version
		 */
    currentVersion: function() {
      var version = this.versionSelect.current;
      return {
        name: version.name,
        resolution: version.resolution,
        bitrate: version.bitrate
      };
    },
    /**
		 * returns a list of available versions based on theme
		 *
		 * @returns {array} array of objects with following format:
		 *		@param {string} name version name (for use with changing current version with player.version() )
		 *		@param {int} resolution the height resolution (i.e. 720p, 360p)
		 *		@param {int} bitrate the approximate bitrate of video
		 *
		 */
    getVersions: function() {
      for (var out = [], i = 0, version; version = this.versionSelect.versions[i]; i++) {
        version.error || out.push({
          name: version.name,
          resolution: version.resolution,
          bitrate: version.bitrate
        });
      }
      return out;
    },
    /**
		 * change the current version quality
		 * switches to a different video version (if possible) and resumes from current point.
		 */
    setVersion: function(versionName) {
      // skip if same version
      if (versionName && this.versionSelect.currentName == versionName) {
        vboss.util.handleError("current version is already " + versionName);
      } else {
        var self = this, $newVideoEl = this._createVideoElement(), $oldVideoEl = this.$el, canPlayPromise = this._updateSource($newVideoEl, this.currentTime(), versionName);
        canPlayPromise.done(function() {
          var swapPromise = $.Deferred(), paused = $oldVideoEl[0].paused, swapEvent = paused ? "suspend" : "timeupdate";
          $newVideoEl[0].play();
          // once new version has buffered or frame is available swap
          $newVideoEl.one("suspend timeupdate", swapPromise.resolve);
          // or swap after 4 seconds if neither occurs
          setTimeout(swapPromise.resolve, 3500);
          swapPromise.done(function() {
            //self.metrics.addEventHandlers($newVideoEl);
            // delay swap slightly to allow buffer to make up for load delay
            setTimeout($.proxy(self, "_swapVideoElements", $newVideoEl, $oldVideoEl), 500);
          });
        }).fail(function() {
          vboss.util.handleError("unable to swap version, error occured");
          $newVideoEl.remove();
        });
      }
    },
    _swapVideoElements: function($currentEl, $previousEl) {
      var previousEl = $previousEl[0], currentEl = $currentEl[0], startTime = previousEl.currentTime;
      this.$el = $currentEl;
      var x = $previousEl.replaceWith($currentEl);
      this._removeEventHandlers($previousEl);
      previousEl.pause();
      previousEl.src = null;
      $previousEl.remove();
      // may have been slight delay while loading, so update to latest
      if (startTime && startTime > 0) {
        currentEl.currentTime = startTime;
        currentEl.play();
      }
      this.metrics.addEventHandlers(currentEl);
      this._addEventHandlers($currentEl);
      vboss.util.publish("version", this.versionSelect.current);
    },
    /**
		 * private function to set current src url, time
		 */
    _updateSource: function($el, startTime, versionName) {
      var self = this, versionSelect = this.versionSelect, el = $el[0], canPlayPromise = new $.Deferred();
      !isNaN(startTime) || (startTime = 0);
      try {
        versionName && versionSelect.update(versionName);
        if (!versionSelect.currentSrc) {
          throw new Error("no available version to play");
        }
      } catch (e) {
        vboss.util.handleError(e.message);
        canPlayPromise.reject();
        return canPlayPromise;
      } finally {
        el.src = versionSelect.currentSrc;
      }
      $el.one({
        loadedmetadata: function() {
          startTime && (el.currentTime = startTime);
        },
        canplay: function() {
          canPlayPromise.resolve();
        },
        error: function() {
          canPlayPromise.reject();
        }
      });
      return canPlayPromise;
    },
    /**
		 * create video element and add event handlers
		 */
    _createVideoElement: function() {
      /* init dom */
      var $el = $("<video></video>", {
        id: this.containerId,
        controls: "controls",
        preload: "metadata",
        poster: null,
        src: null,
        css: {
          width: this.options.width,
          height: this.options.height
        }
      });
      $el[0].volume = this.volume;
      this._addErrorHandler($el);
      return $el;
    },
    /**
		 * add event handling to video element
		 */
    _addEventHandlers: function($el) {
      var containerId = this.containerId, el = $el[0];
      $el.on("play pause resume ended seeking seeked timeupdate ended", function(evt) {
        var eventName = evt.type, details = {};
        switch (eventName) {
         case "timeupdate":
          details.currentTime = el.currentTime;
          details.duration = el.duration;
        }
        vboss.util.publish(eventName, details, containerId);
      });
    },
    _removeEventHandlers: function($el) {
      $el.off("play pause resume ended seeking seeked timeupdate ended error");
    },
    _addErrorHandler: function($el) {
      var self = this, el = ($el || this.$el)[0];
      $el.on("error", function(e) {
        var error = el.error;
        if (!error) {
          return vboss.util.handleError("unknown player error");
        }
        switch (error.code) {
         case error.MEDIA_ERR_ABORTED:
          vboss.util.handleError("Video load aborted");
          break;

         case error.MEDIA_ERR_DECODE:
          vboss.util.handleError("Error decoding video");
          break;

         case error.MEDIA_ERR_NETWORK:
          vboss.util.handleError("Error connecting to network");
          break;

         case error.MEDIA_ERR_SRC_NOT_SUPPORTED:
          vboss.util.handleError("src not supported: " + el.src + ", trying alternate version");
          /**
						 * on 'not supported' error (404 not found or browser can't play)
						 * skip current version and load next available version
						 */
          self.versionSelect.failCurrent();
          // autoplay, resuming from current position in video
          self.setVersion();
        }
      });
      // prevent context menu
      $el.on("contextmenu", function() {
        return false;
      });
      // allows keyboard control after clicking on video (can also focus via tab)
      $el.click(function() {
        $el.focus();
      });
      // attach keyboard event handlers
      var _tempStoredVolume = 0;
      // used for mute/unmute
      $el.on("keydown", function(evt) {
        var seekIncrement = 5;
        // seek forward/back at 5 second intervals
        switch (evt.which) {
         case 32:
          // space - play/pause
          el.paused || el.ended ? el.play() : el.pause();
          break;

         case 38:
          // up - volume up
          el.volume = Math.min(el.volume + .1, 1);
          break;

         case 40:
          // down - volume down
          el.volume = Math.max(el.volume - .1, 0);
          break;

         case 77:
         // m - mute/unmute
          case 109:
          if (el.volume > 0) {
            _tempStoredVolume = el.volume;
            el.volume = 0;
          } else {
            el.volume = _tempStoredVolume ? _tempStoredVolume : 1;
          }
          break;

         case 37:
          // left - rewind
          !isNaN(el.duration) && el.duration > 0 && (// seek 5 seconds back
          el.currentTime = Math.max(el.currentTime - self.seekIncrement, 0));
          break;

         case 39:
          // right - fast forward
          !isNaN(el.duration) && el.duration > 0 && !el.ended && (// seek 5 seconds forward
          el.currentTime = Math.min(el.currentTime + self.seekIncrement, el.duration));
          break;

         case 70:
         // F, fullscreen, not compatible with all browsers
          case 102:
          // f, fullscreen
          el.requestFullScreen ? el.requestFullScreen() : el.mozRequestFullScreen ? el.mozRequestFullScreen() : el.webkitRequestFullScreen && el.webkitRequestFullScreen();
        }
        evt.preventDefault();
      });
      $el.on("volumechange", function(evt) {
        self.volume = el.volume;
      });
    },
    /* event control */
    setVolume: function(newVolume) {
      var el = this.$el[0], volume = isNaN(newVolume) ? this.volume : newVolume;
      volume = Math.min(Math.max(newVolume, 0), 1);
      el.volume = volume;
      this.volume = volume;
      return this;
    },
    getVolume: function() {
      return this.$el[0].volume;
    },
    /**
		 * play video
		 */
    play: function() {
      this.$el[0].play();
    },
    /**
		 * pause video
		 */
    pause: function() {
      this.$el[0].pause();
    },
    /**
		 * seek to specified point in video
		 * @param {int} seconds seconds into video
		 */
    seek: function(seconds) {
      var el = this.$el[0];
      isNaN(seconds) || (el.currentTime = Math.min(Math.max(seconds, 0), el.duration));
    },
    /**
		 * get current position into video
		 * @returns {int} current time
		 */
    currentTime: function() {
      return this.$el[0].currentTime;
    },
    /**
		 * get duration of current video
		 * @returns {int} total duration
		 */
    duration: function() {
      return this.$el[0].duration;
    },
    paused: function() {
      return this.$el[0].paused;
    },
    /** 
		 * add event handlers convenience function.  supports all html5 player events
		 * (play, pause, ended, timeupdate)
		 * @param {string} eventName name of event
		 * @param {function} callback function to run on event.
		 */
    on: function(eventName, callback) {
      "undefined" !== typeof console && console.warn && console.warn("WARNING: vboss.player.html5.on method of attaching event handlers is depreciated, as it is not compatible with automatic bitrate switching");
      return this.$el.on(eventName, callback);
    },
    // stop playback and remove dom content
    unload: function() {
      this.loaded = false;
      this.metrics = void 0;
      if (this.$el) {
        this._removeEventHandlers(this.$el);
        this.pause();
        this.$el[0].src = "";
        this.$el.remove();
        this.$el = void 0;
      }
    }
  });
}(vboss, $);

/**
 * wrapper around vboss flash player
 */
vboss.player.flash = function(applicationId, containerId, options) {
  var _this = this, protocol = /https/.test(document.location.protocol) ? "https:" : "http:";
  this.applicationId = applicationId.replace(/-/g, "");
  this.containerId = containerId;
  this.assetId = null;
  this.data = {
    currentTime: 0,
    lastTimeUpdate: null
  };
  this.options = $.extend({}, this.defaultOptions, options);
  this.paused = true;
  this.ended = false;
  this.loaded = false;
  // placeholder to avoid undefined errors
  this.el = {};
  this.addEventHandlers();
  // load swfobject library from CDN if it doesn't already exist
  this.whenSWFObjectLoaded = "undefined" != typeof swfobject && swfobject ? $.when(swfobject) : $.getScript(protocol + "//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js");
  return this;
};

$.extend(vboss.player.flash.prototype, vboss.player.prototype, {
  /**
	 * load an asset.  uses SWFobject
	 * see vboss.player.load for parameters
	 */
  load: function(assetId, options) {
    var _this = this, containerId = this.containerId, whenPlayerLoaded = new $.Deferred();
    // tell listeners that player is available
    options = $.extend({}, this.options, options);
    this.assetId = assetId.replace(/-/g, "");
    // waits for SWFObject to be available if it isn't already
    this.whenSWFObjectLoaded.then(function() {
      swfobject.embedSWF("http://applications.fliqz.com/" + _this.applicationId + ".swf", // swf
      containerId, // target div id
      options.width, // width
      options.height, // height
      "9.0.115", // version
      null, // express install
      {
        at: assetId,
        autoPlayback: options.autoplay,
        audioMute: options.automute
      }, // flashvars
      {
        wmode: options.wmode,
        bgcolor: decodeURIComponent(options.bgcolor),
        allowscriptaccess: "always",
        allowfullscreen: "true",
        allownetworking: "all"
      }, // params
      {
        name: containerId
      }, // attributes
      function() {
        // callback function
        whenPlayerLoaded.resolve();
      });
    });
    whenPlayerLoaded.done(function() {
      _this.el = document.getElementById(containerId);
      _this.loaded = true;
      vboss.util.publish("loadedmetadata");
    });
    return whenPlayerLoaded;
  },
  addEventHandlers: function() {
    var _this = this;
    // add event handler for notification that control surface is enabled
    vboss.util.on("flashReady", function(objectId, duration, bytes) {
      // see if this is the same player instance
      if (_this.containerId == objectId) {
        _this.data.duration = duration;
        $(_this).trigger("ready");
      }
    });
    vboss.util.on("play", function(assetId) {
      if (!assetId || assetId.replace(/-/g, "") == _this.assetId) {
        _this.paused = false;
        _this.ended = false;
        _this.data.lastTimeUpdate = Date.now();
      }
    });
    vboss.util.on("pause", function(assetId) {
      if (!assetId || assetId.replace(/-/g, "") == _this.assetId) {
        if (!_this.paused) {
          var now = Date.now(), prev = _this.data.lastTimeUpdate || now, diff = (now - prev) / 1e3;
          _this.data.currentTime += diff;
          _this.data.lastTimeUpdate = now;
        }
        _this.paused = true;
      }
    });
    vboss.util.on("resume", function(assetId) {
      assetId && assetId.replace(/-/g, "") != _this.assetId || (_this.paused = false);
    });
    vboss.util.on("ended", function(assetId) {
      if (!assetId || assetId.replace(/-/g, "") == _this.assetId) {
        _this.ended = true;
        _this.data.currentTime = _this.data.duration;
      }
    });
    vboss.util.on("timeupdate", function(assetId, time, percent) {
      if (!assetId || assetId.replace(/-/g, "") == _this.assetId) {
        _this.data.currentTime = parseFloat(time);
        _this.data.lastTimeUpdate = Date.now();
      }
    });
  },
  play: function() {
    this.el.play ? this.el.play() : this.el.Update && this.el.Update("play.start");
  },
  pause: function() {
    this.el.pause && this.el.pause();
  },
  paused: function() {
    return this.paused;
  },
  seek: function(seconds) {
    this.el.seek && this.el.seek(seconds);
    this.updateCurrentTime();
  },
  volume: function(volume) {
    if (void 0 == volume) {
      return this.data.volume;
    }
    this.data.volume = volume;
    this.muted && volume > 0 && (this.muted = false);
    this.el.volume && this.el.volume(volume);
  },
  setVolume: function(volume) {
    this.volume.call(this, volume);
  },
  getVolume: function(volume) {
    this.volume.call(this, volume);
  },
  mute: function(muted) {
    if (muted || "undefined" == typeof muted) {
      this.data._lastVolume = this.data.volume || .5;
      this.muted = true;
      this.volume(0);
    } else {
      this.muted = false;
      this.volume(this.data._lastVolume || 1);
    }
  },
  controls: function(showControls) {
    this.el.controls && this.el.controls(!!showControls);
  },
  /**
	 * removes seek ability from control surface.
	 */
  disableSlider: function() {
    this.el.disableSlider && this.el.disableSlider();
  },
  /**
	 *
	 */
  duration: function() {
    return this.data.duration;
  },
  currentTime: function() {
    if (this.paused) {
      return this.data.currentTime;
    }
    var now = Date.now(), prev = this.data.lastTimeUpdate || now, diff = (now - prev) / 1e3;
    this.updateCurrentTime();
    // make sure updated in case of seek
    return this.data.currentTime + diff;
  },
  buffered: function() {
    return this.data.buffered;
  },
  /**
	 * gets duration. async, so must use callback
	 */
  updateDuration: function(callback) {
    var _this = this;
    if (this.el.duration) {
      var tempName = this._asyncFlashCallback(function(duration) {
        _this.data.duration = parseFloat(duration);
        callback && callback(duration);
      });
      this.el.duration(tempName);
    }
  },
  /**
	 * gets current time.  async, so must use callback
	 */
  updateCurrentTime: function(callback) {
    var _this = this;
    if (this.el.currentTime) {
      var tempName = this._asyncFlashCallback(function(_time) {
        var time = parseFloat(_time, 10);
        if (!isNaN(time)) {
          _this.data.currentTime = parseFloat(time, 10);
          _this.data.lastTimeUpdate = Date.now();
        }
        callback && callback(time);
      });
      this.el.currentTime(tempName);
    }
  },
  /**
	 * gets percent of video buffered, as number between 0 and 1.  async, so must use callback
	 */
  updateBufferedPercent: function(callback) {
    var _this = this;
    if (this.el.bufferedPercent) {
      var tempName = this._asyncFlashCallback(function(pct) {
        _this.data.buffered = pct;
        callback && callback(pct);
      });
      this.el.bufferedPercent(tempName);
    }
  },
  /**
	 * private function.  creates temporary variable for registering callbacks for
	 * flash functions
	 */
  _asyncFlashCallback: function(callback) {
    // store callback with temporary name in global namespace (flash doesn't know context)
    var tmpName = "callback" + (268435456 * Math.random() | 0).toString(16);
    vboss.util.temp || (vboss.util.temp = {});
    // on retrieved duration delete temporary variable
    vboss.util.temp[tmpName] = function(duration) {
      callback(duration);
      delete vboss.util.temp[tmpName];
    };
    return "vboss.util.temp['" + tmpName + "']";
  },
  unload: function() {
    $(this.el).remove();
    this.loaded = false;
  }
});

/**
 * wrapper around vboss smartTag embed
 */
vboss.player.rtmp = function(applicationId, containerId, options) {
  var _this = this, protocol = /https/.test(document.location.protocol) ? "https:" : "http:";
  this.applicationId = applicationId.replace(/-/g, "");
  this.containerId = containerId;
  this.assetId = null;
  this.options = $.extend({}, this.defaultOptions, options);
  this.loaded = false;
  this.ended = false;
  this.metrics = new vboss.metrics(applicationId);
  this.metrics.addEventHandlers(this);
  this.sessionRequest = this.metrics.createSession();
  // versionSelect keeps track of available quality options
  //this.versionSelect = new vboss.player.versionSelect(options.theme);
  this._addEventHandlers();
  // load swfobject library from CDN if it doesn't already exist
  this.whenSWFObjectLoaded = "undefined" != typeof swfobject && swfobject ? $.when(swfobject) : $.getScript(protocol + "//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js");
  // metrics support
  return this;
};

vboss.util && !vboss.util.instances && (vboss.util.instances = {});

$.extend(vboss.player.rtmp.prototype, vboss.player.prototype, {
  // uses CDN hosted playerSWF by default.  offerride to use local SWF via the playerSWF option
  playerSWF: (/https/.test(window.self.location.protocol) ? "https:" : "http:") + "//vbrick.com/fliqz/aa146c8f62da4deaab44a2b75064dd67.swf",
  load: function(assetId, options) {
    var _this = this, protocol = /https/.test(window.self.location.protocol) ? "https:" : "http:", containerId = this.containerId, assetLoadRequest, whenPlayerLoaded = new $.Deferred(), //playerUrl = protocol + '//vbrick.com/fliqz/aa146c8f62da4deaab44a2b75064dd67.swf',
    playerUrl = this.playerSWF, srcBaseUrl = "rtmp://cp242912.edgefcs.net/fliqz/mp4:content/", bridgeCallback;
    // if on cdn9 domain then SWF can use javascript control
    /cdn9\.fliqz\.com/.test(window.self.location.hostname) && (playerUrl = "aa146c8f62da4deaab44a2b75064dd67.swf");
    assetId = assetId.replace(/-/g, "");
    options = $.extend({}, this.options, options);
    // add option to select location of player SWF
    options.playerSWF && (playerUrl = options.playerSWF);
    this.assetId = assetId;
    this.assetUrl = null;
    $.when(this.sessionRequest, this.whenSWFObjectLoaded).then(function() {
      _this.metrics.load(assetId).done(function(assetUrl) {
        var streamingUrl, flashvars, fileExtension = (assetUrl.match(/.*\.([^?&]*)/) || [ "mp4" ])[1], timeUpdateInterval = 1e3;
        // 1 second update interval
        // make sure url has proper filetype for instances of flv/f4v
        streamingUrl = srcBaseUrl.replace(/mp4:/, fileExtension + ":");
        // construct rtmp url from returned http url
        streamingUrl += assetUrl.replace("http://cdn9.fliqz.com/", "");
        flashvars = {
          src: streamingUrl,
          poster: "http:" == protocol && "http://previews.fliqz.com/" + assetId + ".jpg",
          controlBarMode: "docked",
          streamType: "recorded",
          bufferingOverlay: false,
          currentTimeUpdateInterval: timeUpdateInterval,
          bytesLoadedUpdateInterval: 1e3,
          javascriptCallbackFunction: _this._generateBridgeCallback(containerId, options),
          autoPlay: options.autoplay,
          muted: options.automute
        };
        // flashvars
        flashvars = $.extend(flashvars, options.flashvars);
        swfobject.embedSWF(playerUrl, // swf
        containerId, // target div id
        options.width, // width
        options.height, // height
        "10.1.0", // version
        null, // express install
        flashvars, {
          wmode: options.wmode,
          bgcolor: decodeURIComponent(options.bgcolor),
          allowscriptaccess: "always",
          allowfullscreen: "true",
          allownetworking: "all"
        }, // params
        {
          name: containerId
        }, // attributes
        function() {
          // callback function
          whenPlayerLoaded.resolve();
        });
      }).fail(function(error) {
        whenPlayerLoaded.reject(error);
      });
    });
    whenPlayerLoaded.done(function() {
      _this.el = document.getElementById(containerId);
      _this.loaded = true;
    });
    return whenPlayerLoaded;
  },
  /**
	 * private function.  creates callback to bind to javascript API for player
	 */
  _generateBridgeCallback: function(containerId, options) {
    var _this = this, $this = $(this), autoplay = options.autoplay, instanceName = "rtmp" + (268435456 * Math.random() | 0).toString(16), bridgeFunctionName = "vboss.util.instances." + instanceName;
    vboss.util.instances || (vboss.util.instances = {});
    vboss.util.instances[instanceName] = function(id, eventName) {
      var details = $.extend({}, arguments ? arguments[2] : {});
      _this.el || (_this.el = document.getElementById(id));
      // allow seeking on initial load
      if (options.startTime && !_this.startTime) {
        _this.startTime = options.startTime;
        $this.one("loadedmetadata", function() {
          if (_this.startTime && _this.startTime > 0) {
            _this.seek.call(_this, _this.startTime);
            _this.startTime = void 0;
            $this.one("seeked", function() {
              _this.play();
            });
          }
        });
      }
      switch (eventName) {
       case "timeupdate":
        isNaN(details.currentTime) || $this.trigger("timeupdate", details);
        break;

       case "durationchange":
        isNaN(details.duration) || $this.trigger("durationchange", details);
        break;

       case "onJavaScriptBridgeCreated":
        break;

       case "loadedmetadata":
       case "pause":
       case "seeking":
       case "seeked":
        $this.trigger(eventName);
        break;

       case "play":
        _this.ended && (_this.ended = false);
        $this.trigger(eventName);
        break;

       case "ended":
       case "complete":
        _this.ended = true;
        $this.trigger(eventName);
        break;

       case "progress":
       case "emptied":
       case "waiting":
        break;

       // errors
        case _this.errorCodes.NETSTREAM_STREAM_NOT_FOUND:
       case _this.errorCodes.HTTP_GET_FAILED:
       case _this.errorCodes.MEDIA_LOAD_FAILED:
       case _this.errorCodes.NETCONNECTION_REJECTED:
       case _this.errorCodes.NETCONNECTION_TIMEOUT:
       case _this.errorCodes.NETSTREAM_PLAY_FAILED:
       case _this.errorCodes.NETSTREAM_NO_SUPPORTED_TRACK_FOUND:
        $this.trigger("error", {
          message: details
        });
      }
    };
    return bridgeFunctionName;
  },
  _updateSource: function(url, startTime) {
    var _this = this;
    startTime && (_this.startTime = startTime);
    _this.el.src = url;
    // this will trigger 'loadedmetadata' once loaded and set start time
    _this.el.load && _this.el.load();
  },
  _addEventHandlers: function() {
    var containerId = this.containerId;
    $(this).on("play pause resume ended seeking seeked timeupdate ended error", function(evt, details) {
      var eventName = evt.type;
      vboss.util.publish(eventName, details, containerId);
    });
  },
  _removeEventHandlers: function() {
    $(this).off("play pause resume ended seeking seeked timeupdate ended error");
  },
  /* event control */
  setVolume: function(newVolume) {
    var volume = isNaN(newVolume) ? this.volume : newVolume;
    volume = Math.min(Math.max(newVolume, 0), 1);
    this.el.setVolume && this.el.setVolume(volume);
    this.volume = volume;
    return this;
  },
  getVolume: function() {
    return this.el.getMuted && this.el.getMuted() ? 0 : this.el.getVolume ? this.el.getVolume() : void 0;
  },
  /**
	 * play video
	 */
  play: function() {
    this.el.play2 && this.el.play2();
  },
  /**
	 * pause video
	 */
  pause: function() {
    this.el.pause && this.el.pause();
  },
  /**
	 * seek to specified point in video
	 * @param {int} seconds seconds into video
	 */
  seek: function(seconds) {
    var el = this.el;
    !isNaN(seconds) && el.canSeekTo && el.canSeekTo(seconds) && el.seek(seconds);
  },
  /**
	 * get current position into video
	 * @returns {int} current time
	 */
  currentTime: function() {
    return this.el.getCurrentTime ? this.el.getCurrentTime() : 0;
  },
  /**
	 * get duration of current video
	 * @returns {int} total duration
	 */
  duration: function() {
    return this.el.getDuration ? this.el.getDuration() : void 0;
  },
  paused: function() {
    return this.el.getState ? "paused" === this.el.getState() : true;
  },
  unload: function() {
    this.metrics = void 0;
    this._removeEventHandlers();
    this.loaded = false;
    if (this.el) {
      this.pause();
      this.el.src = "";
      $(this.el).remove();
      this.el = void 0;
    }
  },
  errorCodes: {
    IO_ERROR: 1,
    SECURITY_ERROR: 2,
    ASYNC_ERROR: 3,
    URL_SCHEME_INVALID: 5,
    HTTP_GET_FAILED: 6,
    MEDIA_LOAD_FAILED: 7,
    SOUND_PLAY_FAILED: 10,
    NETCONNECTION_REJECTED: 11,
    NETCONNECTION_APPLICATION_INVALID: 12,
    NETCONNECTION_FAILED: 13,
    NETCONNECTION_TIMEOUT: 14,
    NETSTREAM_PLAY_FAILED: 15,
    NETSTREAM_STREAM_NOT_FOUND: 16,
    NETSTREAM_FILE_STRUCTURE_INVALID: 17,
    NETSTREAM_NO_SUPPORTED_TRACK_FOUND: 18
  }
});

/**
 * create playlist object
 *
 * @param {guid} applicationId id of player to use
 * @param {guid=} collectionId collection to load.  if not specified playlist shows all videos in account (optional)
 * @param {object=} additional options
 */
vboss.playlist = function(applicationId, collectionId, options) {
  if (!(this instanceof vboss.playlist)) {
    return new vboss.playlist(applicationId, collectionsId, options);
  }
  var _this = this;
  this.applicationId = applicationId;
  this.collectionId = collectionId;
  this.pagesize = 8;
  this.page = 1;
  this.containerId = "video";
  this.assets = [];
  this.initNavigation();
  this.calculations = this.calculatePlaylistGrid();
  this.pagesize = this.calculations.pagesize;
  this.options = $.extend({}, this.defaultOptions, options, {
    page: this.page,
    pagesize: this.pagesize
  });
  this.player = new vboss.player(applicationId, this.containerId, this.options);
  var request;
  // if collectionId isn't set then just return results from entire library
  request = collectionId ? vboss.search.collection(collectionId, this.options) : vboss.search.query(applicationId, this.options);
  request.done(function(assets) {
    var assetId;
    // if only one asset is found then wrap as array
    if (!$.isArray(assets)) {
      assets = [ assets ];
      assets.itemTotal = 1;
      assets.pages = 1;
      assets.page = 1;
    }
    _this.parse.call(_this, assets);
    _this.updateNavigation.call(_this, assets);
    _this.assets = assets;
    assetId = assets[0].id;
    (false !== options.preload || options.autoplay) && _this.player.load(assetId);
    vboss.util.publish("loadedplaylist");
  });
  return this;
};

vboss.playlist.prototype.defaultOptions = {
  preferHTML5: true,
  smartTagFallback: false
};

/**
 * preferences for setting up playlist items.
 * grabberWidth is width (in pixels) of side bar, others are for sizing of playlist cells
 */
vboss.playlist.prototype.dimensions = {
  grabberWidth: 80,
  preferredItemWidth: 320,
  preferredItemHeight: 180,
  min_cols: 2,
  min_rows: 2,
  max_cols: 5,
  max_rows: 4
};

/**
 * set up playlist dimensions and number of cells
 */
vboss.playlist.prototype.calculatePlaylistGrid = function() {
  var $el = $("#main"), grabberWidth = $("#nav").width(), width = $el.width() - grabberWidth, height = $el.height(), dpi = window.devicePixelRatio || 1, dims = this.dimensions, cols = Math.round(width / (dims.preferredItemWidth * dpi)), rows = Math.floor(height / (dims.preferredItemHeight * dpi)), cellWidth, cellHeight;
  cols = Math.min(Math.max(dims.min_cols, cols), dims.max_cols);
  // limit to max
  rows = Math.min(Math.max(dims.min_rows, rows), dims.max_rows);
  cellWidth = Math.ceil(width / cols);
  cellHeight = Math.ceil(height / rows);
  this.pageSize = cols * rows;
  return {
    pagesize: cols * rows,
    cols: cols,
    rows: rows,
    cellWidth: cellWidth,
    cellHeight: cellHeight
  };
};

/**
 * setup navigation click handlers
 */
vboss.playlist.prototype.initNavigation = function() {
  var _this = this;
  this.isOpen = false;
  $("#open").on("click", function() {
    _this.togglePlaylist.call(_this);
  });
};

/**
 * show or hide playlist
 */
vboss.playlist.prototype.togglePlaylist = function() {
  var isOpen = this.isOpen, openBtn = $("#open"), navBtns = $("#prev, #next, #position"), vid = $("#video"), list = $("#list-container");
  if (isOpen) {
    list.hide();
    openBtn.addClass("button-full");
    navBtns.hide();
    this.toggleVideo(true);
  } else {
    this.player.loaded && this.player.pause && this.player.pause();
    this.toggleVideo(false);
    openBtn.removeClass("button-full");
    navBtns.show();
    list.show();
  }
  this.isOpen = !this.isOpen;
};

vboss.playlist.prototype.toggleVideo = function(isVisible) {
  // display: none reloads video content
  this.player instanceof vboss.player.flash || this.player instanceof vboss.player.rtmp ? $("#video").toggleClass("flash-hidden", !isVisible) : $("#video").toggleClass("html5-hidden", !isVisible);
};

/**
 * parse results from API and display
 */
vboss.playlist.prototype.parse = function(assets) {
  var _this = this, template = $("#item-template").html(), containerId = "mediaPlayer", list = $("#list");
  list.empty();
  // empty previous results
  var dimensions = this.calculatePlaylistGrid();
  // go through and add entry for each asset
  $(assets).each(function(i, asset) {
    var id = asset.id.replace(/-/g, ""), item = $(template);
    // populate data
    item.find(".title").text(asset.title);
    item.find(".description").text(asset.description);
    item.find(".date").text(prettifyDate(asset.date));
    item.find(".duration").text(prettifyDuration(asset.metrics.duration));
    item.css({
      width: (100 / dimensions.cols).toPrecision(5) + "%",
      height: (100 / dimensions.rows).toPrecision(5) + "%"
    });
    item.find(".item").attr("id", id).css({
      background: [ "url('http://previews.fliqz.com/", id, ".jpg?width=", dimensions.cellWidth, "') no-repeat center" ].join(""),
      backgroundSize: "100% auto"
    });
    item.on("click tap", function() {
      _this.player.load(asset.id, {
        autoplay: true,
        startTime: 0
      });
      _this.togglePlaylist.call(_this);
    });
    list.append(item);
  });
  // helper function to take a duration in seconds and turn it into hour:minute:second format
  function prettifyDuration(duration) {
    var pad2 = function(x) {
      x = Math.floor(x % 60).toFixed();
      return "0000".substr(0, 2 - x.length) + x;
    };
    // zero pad digits
    return [ duration >= 3600 ? pad2(duration / 3600) + ":" : "", pad2(duration / 60), ":", pad2(duration) ].join("");
  }
  // helper function to convert date to string
  function prettifyDate(date) {
    var d = new Date(date), _zeroPad = function(x, digits) {
      return "0000".substr(0, digits - x.toString().length) + x.toString();
    };
    return [ _zeroPad(d.getMonth() + 1, 2), _zeroPad(d.getDate(), 2), d.getFullYear() ].join("/");
  }
};

/**
 * update prev/next buttons to point to updated feed
 */
vboss.playlist.prototype.updateNavigation = function(assets) {
  var _this = this, text = assets.page + " of " + assets.pages;
  $("#position em").text(text);
  //updateLink('#first', assets.first);
  updateLink("#prev", assets.prev);
  updateLink("#next", assets.next);
  //updateLink('#last', assets.last);
  // helper function to update link buttons
  function updateLink(linkId, action) {
    var el = $(linkId);
    el.off("click");
    // remove existing action
    $("em", el).toggleClass("disabled", void 0 == action);
    action && el.on("click", function(evt) {
      evt.preventDefault();
      var request = action();
      request.done(function(assets) {
        _this.parse.call(_this, assets);
        _this.updateNavigation.call(_this, assets);
      });
    });
  }
};