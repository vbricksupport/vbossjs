// Internet Explorer polyfill for isostring
if (!Date.prototype.toISOString) {
  (function () {
     
    function pad(num) {
      var r = num.toFixed();
      if (r.length === 1) {
        r = '0' + r;
      }
      return r;
    }
  
    Date.prototype.toISOString = function () {
      return [
				this.getUTCFullYear(),
        '-' , pad(this.getUTCMonth() + 1),
        '-' , pad(this.getUTCDate()),
        'T' , pad(this.getUTCHours()),
        ':' , pad(this.getUTCMinutes()),
        ':' , pad(this.getUTCSeconds()),
        '.' , (this.getUTCMilliseconds()/1000).toFixed(3).toString().slice(2, 5),
        'Z'
			].join('');
    };
   
  }());
}

// use XDomainRequest for IE, to allow CORS
// adapted from 
// https://github.com/jaubourg/ajaxHooks/blob/master/src/xdr.js
if ( window.XDomainRequest ) {
	$.ajaxTransport(function( s ) {
		if ( s.crossDomain && s.async ) {
			if ( s.timeout ) {
				s.xdrTimeout = s.timeout;
				delete s.timeout;
			}
			var xdr;
			
			return {
				send: function( _, complete ) {
					function callback( status, statusText, responses, responseHeaders ) {
						xdr.onload = xdr.onerror = xdr.ontimeout = $.noop;
						xdr = undefined;
						
						complete( status, statusText, responses, responseHeaders );
					}
					xdr = new XDomainRequest();
					
					xdr.onload = function() {
						var responses = { text: xdr.responseText };
						
						// include parsed xml if valid
						try {
							responses.xml = $.parseXML(xdr.responseText);
						} catch(e) {}
						
						callback( 200, "OK", responses, "Content-Type: " + xdr.contentType );
					};
					xdr.onerror = function() {
						callback( 404, "Not Found" );
					};
					xdr.onprogress = $.noop;
					xdr.ontimeout = function() {
						callback( 0, "timeout" );
					};
					xdr.timeout = s.xdrTimeout || Number.MAX_VALUE;
					xdr.open( s.type, s.url );
					setTimeout(function() {
						xdr.send( ( s.hasContent && s.data ) || null );
					}, 0);
				},
				abort: function() {
					if ( xdr ) {
						xdr.onerror = $.noop;
						xdr.abort();
					}
				}
			};
		}
	});
};