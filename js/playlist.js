
/**
 * create playlist object
 *
 * @param {guid} applicationId id of player to use
 * @param {guid=} collectionId collection to load.  if not specified playlist shows all videos in account (optional)
 * @param {object=} additional options
 */
vboss.playlist = function(applicationId, collectionId, options) {
	if(!(this instanceof vboss.playlist)) return new vboss.playlist(applicationId, collectionsId, options);
	
	var _this = this;
	this.applicationId = applicationId;
	this.collectionId = collectionId;
	this.pagesize = 8;
	this.page = 1;
	this.containerId = 'video';
	this.assets = [];
	
	
	this.initNavigation();
	
	this.calculations = this.calculatePlaylistGrid();

	this.pagesize = this.calculations.pagesize;
	
	this.options = $.extend({}, this.defaultOptions, options, {
		page: this.page,
		pagesize: this.pagesize
	});
	
	this.player = new vboss.player(applicationId, this.containerId, this.options);
	
	var request;

	// if collectionId isn't set then just return results from entire library
	if(collectionId) request = vboss.search.collection(collectionId, this.options);
	else request = vboss.search.query(applicationId, this.options);
	
	request.done(function(assets) {
		var assetId;
		
		// if only one asset is found then wrap as array
		if(!$.isArray(assets)) {
			assets = [assets];
			assets.itemTotal = 1;
			assets.pages = 1;
			assets.page = 1;
		}
		
		_this.parse.call(_this, assets);
		_this.updateNavigation.call(_this, assets);
		_this.assets = assets;
		
		assetId = assets[0].id;
		
		if ((options.preload === false) && !options.autoplay) {
		
		} else {
			_this.player.load(assetId);
		}
		
		vboss.util.publish('loadedplaylist');
	});
	
	return this;
}

vboss.playlist.prototype.defaultOptions = {
	preferHTML5: true,
	smartTagFallback: false
};

/**
 * preferences for setting up playlist items.
 * grabberWidth is width (in pixels) of side bar, others are for sizing of playlist cells
 */
vboss.playlist.prototype.dimensions = {
	grabberWidth: 80,
	preferredItemWidth: 320,
	preferredItemHeight: 180,
	min_cols: 2,
	min_rows: 2,
	max_cols: 5,
	max_rows: 4
};


/**
 * set up playlist dimensions and number of cells
 */
vboss.playlist.prototype.calculatePlaylistGrid = function() {
	var $el = $('#main'),
			grabberWidth = $('#nav').width(),
			width = $el.width() - grabberWidth,
			height = $el.height(),
			dpi = window.devicePixelRatio || 1.0,
			dims = this.dimensions,
			cols = Math.round(width / (dims.preferredItemWidth*dpi)),
			rows = Math.floor(height / (dims.preferredItemHeight*dpi)),
			cellWidth, cellHeight;
	
	cols = Math.min(Math.max(dims.min_cols, cols), dims.max_cols); // limit to max
	rows = Math.min(Math.max(dims.min_rows, rows), dims.max_rows);
		
	cellWidth = Math.ceil(width / cols);
	cellHeight = Math.ceil(height / rows);
		
	this.pageSize = cols*rows;
	
	return {
		pagesize: cols*rows,
		cols: cols,
		rows: rows,
		cellWidth: cellWidth,
		cellHeight: cellHeight
	};
		
};

/**
 * setup navigation click handlers
 */
vboss.playlist.prototype.initNavigation = function() {
	var _this = this;
	
	this.isOpen = false;
	
	$('#open').on('click', function() {
		_this.togglePlaylist.call(_this); // bind to playlist
	});
};

/**
 * show or hide playlist
 */
vboss.playlist.prototype.togglePlaylist = function() {
	var isOpen = this.isOpen;

	var openBtn = $('#open');
	var navBtns = $('#prev, #next, #position');
	
	var vid = $('#video');
	var list = $('#list-container');
	
	if(isOpen) {
		list.hide();
		
		openBtn.addClass('button-full');
		navBtns.hide();
		
		this.toggleVideo(true);
	
	} else {
		
		if (this.player.loaded && this.player.pause) {
			this.player.pause();
		}
		this.toggleVideo(false);
		
		openBtn.removeClass('button-full');
		navBtns.show();
				
		list.show();
	}
	
	this.isOpen = !this.isOpen;
};

vboss.playlist.prototype.toggleVideo = function(isVisible) {
	// display: none reloads video content
	if (this.player instanceof vboss.player.flash || this.player instanceof vboss.player.rtmp) {
		$('#video').toggleClass('flash-hidden', !isVisible);
	} else {
		$('#video').toggleClass('html5-hidden', !isVisible);
	}
}

/**
 * parse results from API and display
 */
vboss.playlist.prototype.parse = function(assets) {
	var _this = this;
	var template = $('#item-template').html(); // use template html to constrict each gallery entry
	var containerId = 'mediaPlayer'; // id of DIV where the video will be embedded when an item is clicked
	
	var list = $('#list'); 
	list.empty(); // empty previous results
	
	var dimensions = this.calculatePlaylistGrid();
	// go through and add entry for each asset
	$(assets).each(function(i, asset) {
		var id = asset.id.replace(/-/g,'');
		
		var item = $(template);
		// populate data
		item.find('.title').text(asset.title);
		item.find('.description').text(asset.description);
		item.find('.date').text( prettifyDate(asset.date) );
		item.find('.duration').text( prettifyDuration(asset.metrics.duration) );
		
		item.css({
				width: (100/dimensions.cols).toPrecision(5) + '%',
				height: (100/dimensions.rows).toPrecision(5) + '%'
		});
		
		item.find('.item')
			.attr('id', id)
			.css({
				background: ["url('http://previews.fliqz.com/", id, ".jpg?width=", dimensions.cellWidth,"') no-repeat center"].join(''),
				backgroundSize: '100% auto',
			});
			
		item.on('click tap', function() {
			_this.player.load(asset.id, {autoplay: true, startTime: 0});
			_this.togglePlaylist.call(_this);
		});
		
		list.append(item); // add to list
	
	});
	
	// helper function to take a duration in seconds and turn it into hour:minute:second format
	function prettifyDuration(duration) {
		var pad2 = function(x) { x=Math.floor(x%60).toFixed(); return "0000".substr(0,2-x.length) + x; }; // zero pad digits
		return [(duration>=3600) ? pad2(duration/3600) + ':' : '', pad2(duration/60), ':',	pad2(duration)].join('');
	}
	
	// helper function to convert date to string
	function prettifyDate(date) {
		var d = new Date(date);
		var _zeroPad = function(x,digits) { return "0000".substr(0,digits-x.toString().length) + x.toString(); }
		return [_zeroPad(d.getMonth()+1,2), _zeroPad(d.getDate(), 2), d.getFullYear()].join('/');
	}

};

/**
 * update prev/next buttons to point to updated feed
 */
vboss.playlist.prototype.updateNavigation = function(assets) {
	var _this = this;
	// update navigation details
	// the assets array includes additional information about the search
	var text = assets.page + ' of ' + assets.pages;
	$('#position em').text(text);
	
	//updateLink('#first', assets.first);
	updateLink('#prev', assets.prev);
	updateLink('#next', assets.next);
	//updateLink('#last', assets.last);
	
	// helper function to update link buttons
	function updateLink(linkId, action) {
		var el = $(linkId);
		
		el.off('click'); // remove existing action
		$('em', el).toggleClass('disabled', (action == undefined) );
		
		if(action) el.on('click', function(evt) { 
			evt.preventDefault();
		
			var request = action();
			request.done(function(assets) {
				_this.parse.call(_this, assets);
				_this.updateNavigation.call(_this, assets);
			});
			
			//trackProgress(request); // show status of request
		}); // load link when clicked	
	};
};