
var ugly = require('uglify-js');
var util = require('util');
var fs = require('fs');

var files = [
	'util.js',
	'polyfills.js',
	'services/asset.js',
	'services/search.js',
	'services/metrics.js',
	'player.js',
	'player/versionSelect.js',
	'player/smartTag.js',
	'player/html5.js',
	'player/flash.js',
	'player/rtmp.js',
	'playlist.js'
];

var toplevel = null;
files.forEach(function(file){
    var code = fs.readFileSync(file);
		try {
			toplevel = ugly.parse(code.toString(), {
					filename: file,
					toplevel: toplevel
			});
		} catch (e) {
			console.error(file, e);
			throw(e);
		}
});

console.log('figure out scope');
toplevel.figure_out_scope();

var compressOptions = {
	sequences: false,
	properties: true,
	dead_code: false,
	drop_debugger: true,
	unsafe: false,
	conditionals: true,
	comparisons: false,
	evaluate: true,
	booleans: false,
	unused: false,
	hoist_funs: false,
	hoist_vars: false,
	if_return: true,
	join_vars: true,
	cascade: true,
	warnings: true
	
};


console.log('compress');
var compressor = ugly.Compressor(compressOptions);
var compressed = toplevel.transform(compressor);

compressed.figure_out_scope();
compressed.compute_char_frequency();


var options = {
	indent_start  : 0,
	indent_level  : 2,
	quote_keys    : false,
	space_colon   : true,
	ascii_only    : false,
	inline_script : false,
	width         : 80,
	max_line_len  : 32000,
	beautify      : true,
	source_map    : null,
	bracketize    : true,
	semicolons    : true,
	comments      : function(main, comment) {
			//if(comment.value.match(/@file|require\(/)) return false;
			if(comment.value.match(/TODO|require\(/)) return false;
			else return true;
		},
	preserve_line : false
};

console.log('output');
var output, code;

try {
	output = ugly.OutputStream(options);
} catch(e) {
	console.error(e);
	throw(e);
}
compressed.print(output);
code = output.toString();

console.log('write vboss.js');
fs.writeFileSync('vboss.js', output.toString());

/* minified version */

var options = {
	indent_start  : 0,
	indent_level  : 2,
	quote_keys    : false,
	space_colon   : true,
	ascii_only    : false,
	inline_script : false,
	width         : 80,
	max_line_len  : 32000,
	beautify      : false,
	source_map    : null,
	bracketize    : true,
	semicolons    : true,
	comments      : false,
	preserve_line : false
};

console.log('output min');
try {
	output = ugly.OutputStream(options);
} catch(e) {
	console.error(e);
	throw(e);
}
compressed.print(output);
code = output.toString();

console.log('write min');
fs.writeFileSync('vboss.min.js', output.toString());

