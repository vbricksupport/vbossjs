/**
 * wrapper around vboss smartTag embed
 */
vboss.player.smartTag = function(applicationId, containerId, options) {
	var _this = this;
	
	this.applicationId = applicationId.replace(/-/g, '');
	this.containerId = containerId;
	this.assetId = null;
	
	this.options = $.extend({}, this.defaultOptions, options);
	
	return this;
};

$.extend(vboss.player.smartTag.prototype, vboss.player.prototype, {
	load: function(assetId, options) {
		options = $.extend({}, this.options, options);
		
		// load smart tag
		var protocol = /https/.test(document.location.protocol) ? 'https:' : 'http:',
				smartTagUrl = protocol + '//services.fliqz.com/smart/20100401/' + 
						'applications/' + this.applicationId +
						'/assets/' + assetId +
						'/containers/' + this.containerId +
						'/smarttag.js';
		
		this.assetId = assetId;
		
		return $.ajax({
			url: smartTagUrl,
			data: {
				width: options.width || '100%',
				height: options.height || '100%',
				autoPlayback: options.autoplay || false,
				audioMute: options.automute || false,
				bgcolor: decodeUriComponent(options.bgcolor || '#000000')
			},
			dataType: 'script',
			cache: 'true'
		});
	}
});