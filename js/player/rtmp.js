/**
 * wrapper around vboss smartTag embed
 */
vboss.player.rtmp = function(applicationId, containerId, options) {
	var _this = this,
			protocol = /https/.test(document.location.protocol) ? 'https:' : 'http:';
	
	this.applicationId = applicationId.replace(/-/g, '');
	this.containerId = containerId;
	this.assetId = null;
	this.options = $.extend({}, this.defaultOptions, options);
	
	this.loaded = false;
	this.ended = false;
	
	// TODO - metrics is set to listen to video element handlers, this is wrapping the player object instead of element.  is that best behavior?
	this.metrics = new vboss.metrics(applicationId);
	this.metrics.addEventHandlers(this);
	this.sessionRequest = this.metrics.createSession();
	// versionSelect keeps track of available quality options
	// TODO 
	//this.versionSelect = new vboss.player.versionSelect(options.theme);
	
	this._addEventHandlers();
	
	// load swfobject library from CDN if it doesn't already exist
	this.whenSWFObjectLoaded = (typeof swfobject != 'undefined' && swfobject)
		? $.when(swfobject) // immediate
		: $.getScript(protocol + '//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js');
	
	// metrics support
	
	return this;
};

if (vboss.util && !vboss.util.instances) {
	vboss.util.instances = {};
};

$.extend(vboss.player.rtmp.prototype, vboss.player.prototype, {
	// uses CDN hosted playerSWF by default.  offerride to use local SWF via the playerSWF option
	playerSWF: (/https/.test(window.self.location.protocol) ? 'https:' : 'http:') + '//vbrick.com/fliqz/aa146c8f62da4deaab44a2b75064dd67.swf',

	load: function(assetId, options) {
		var _this = this,
			protocol = /https/.test(window.self.location.protocol) ? 'https:' : 'http:',
			containerId = this.containerId,
			assetLoadRequest,
			whenPlayerLoaded = new $.Deferred(),
			//playerUrl = protocol + '//vbrick.com/fliqz/aa146c8f62da4deaab44a2b75064dd67.swf',
			playerUrl = this.playerSWF,
			srcBaseUrl = 'rtmp://cp242912.edgefcs.net/fliqz/mp4:content/',
			bridgeCallback;
		
		// if on cdn9 domain then SWF can use javascript control
		if (/cdn9\.fliqz\.com/.test(window.self.location.hostname)) {
			playerUrl = 'aa146c8f62da4deaab44a2b75064dd67.swf';
		}
		
		assetId = assetId.replace(/-/g, '');
		options = $.extend({}, this.options, options);
		
		// add option to select location of player SWF
		if (options.playerSWF) playerUrl = options.playerSWF;
		
		this.assetId = assetId;
		this.assetUrl = null;
		
		$.when(this.sessionRequest,this.whenSWFObjectLoaded).then(function () {
			_this.metrics.load(assetId).done(function (assetUrl) {
				var streamingUrl,
					flashvars,
					fileExtension = (assetUrl.match(/.*\.([^?&]*)/) || ['mp4'])[1],
					timeUpdateInterval = 1 * 1000; // 1 second update interval
				
				// make sure url has proper filetype for instances of flv/f4v
				streamingUrl = srcBaseUrl.replace(/mp4:/,fileExtension + ':');
				// construct rtmp url from returned http url
				streamingUrl += assetUrl.replace('http://cdn9.fliqz.com/', '');
				
				flashvars = {
					src: streamingUrl,
					poster: (protocol == 'http:')  && ('http://previews.fliqz.com/' + assetId + '.jpg'),
					controlBarMode: 'docked',
					streamType: 'recorded',
					bufferingOverlay: false,
					currentTimeUpdateInterval: timeUpdateInterval,
					bytesLoadedUpdateInterval: 1000,
					javascriptCallbackFunction: _this._generateBridgeCallback(containerId, options),
					autoPlay: options.autoplay,
					muted: options.automute
				}; // flashvars
				
				flashvars = $.extend(flashvars, options.flashvars);
				
				swfobject.embedSWF(
					playerUrl, // swf
					containerId, // target div id
					options.width, // width
					options.height, // height
					'10.1.0', // version
					null, // express install
					flashvars,
					{
						wmode: options.wmode,
						bgcolor: decodeURIComponent(options.bgcolor),
						allowscriptaccess: 'always',
						allowfullscreen: 'true',
						allownetworking: 'all'
					}, // params
					{	name: containerId	}, // attributes
					function() { // callback function
						whenPlayerLoaded.resolve();
					}
				);
			}).fail(function (error) {
				whenPlayerLoaded.reject(error);
			});
		});
		
		
		whenPlayerLoaded.done(function() {
			_this.el = document.getElementById(containerId);
			_this.loaded = true;
		});
		
		return whenPlayerLoaded;
	},
	/**
	 * private function.  creates callback to bind to javascript API for player
	 */
	_generateBridgeCallback: function(containerId, options) {
		var _this = this,
			$this = $(this),
			autoplay = options.autoplay,
			instanceName = 'rtmp' + (Math.random()*0x10000000|0).toString(16),
			bridgeFunctionName = 'vboss.util.instances.' + instanceName;
		
		if(!vboss.util.instances) vboss.util.instances = {}; 
		
		vboss.util.instances[instanceName] = function(id, eventName) {
			var details = $.extend({}, (arguments) ? arguments[2] : {});
			
			if (!_this.el) {
				_this.el = document.getElementById(id);
			}
			
			// allow seeking on initial load
			if (options.startTime && !_this.startTime) {
				_this.startTime = options.startTime;
				
				$this.one('loadedmetadata', function () {
					if (_this.startTime && (_this.startTime > 0)) {
						
						_this.seek.call(_this, _this.startTime);
						_this.startTime = undefined;
						
						$this.one('seeked', function () {
							_this.play();
						});
					}
				});
			}
			
			switch(eventName) {
				case 'timeupdate':
					if (!isNaN(details.currentTime)) {
						$this.trigger('timeupdate', details);
					} 
					break;
				case 'durationchange':
					if (!isNaN(details.duration)) {
						$this.trigger('durationchange', details);
					}
					break;
				case 'onJavaScriptBridgeCreated':
					break;
				case 'loadedmetadata':
				case 'pause':
				case 'seeking':
				case 'seeked':
					$this.trigger(eventName);
					break;
				case 'play':
					if (_this.ended) _this.ended = false;
					$this.trigger(eventName);
					break;
				case 'ended':
				case 'complete':
					_this.ended = true;
					$this.trigger(eventName);
					break;
				case 'progress':
				case 'emptied':
				case 'waiting':
					break;
				// errors
				case _this.errorCodes.NETSTREAM_STREAM_NOT_FOUND:
				case _this.errorCodes.HTTP_GET_FAILED:
				case _this.errorCodes.MEDIA_LOAD_FAILED:
				case _this.errorCodes.NETCONNECTION_REJECTED:
				case _this.errorCodes.NETCONNECTION_TIMEOUT:
				case _this.errorCodes.NETSTREAM_PLAY_FAILED:
				case _this.errorCodes.NETSTREAM_NO_SUPPORTED_TRACK_FOUND:
					$this.trigger('error', {message: details});
					break;
				
			}
		};
		
		
		return bridgeFunctionName;
		
	},
	_updateSource: function (url, startTime) {
		var _this = this;
		
		if (startTime) {
			_this.startTime = startTime;
		}
		
		_this.el.src = url;
		// this will trigger 'loadedmetadata' once loaded and set start time
		if (_this.el.load) _this.el.load();
	},
	_addEventHandlers: function () {
		var containerId = this.containerId;
		
		$(this).on('play pause resume ended seeking seeked timeupdate ended error', function(evt, details) {
				var eventName = evt.type;
				vboss.util.publish(eventName, details, containerId);
		});
	},
	_removeEventHandlers: function () {
		$(this).off('play pause resume ended seeking seeked timeupdate ended error');
	},
	/* event control */
	setVolume: function (newVolume) {
		var	volume = !isNaN(newVolume) ? newVolume : this.volume;
		
		volume = Math.min(Math.max(newVolume, 0), 1.0);
		if (this.el.setVolume) this.el.setVolume(volume);
		this.volume = volume;
		return this;
	},
	getVolume: function() { 
		if (this.el.getMuted && this.el.getMuted()) {
			return 0;
		} else if (this.el.getVolume) {
			return this.el.getVolume();
		}
	},
	/**
	 * play video
	 */
	play: function() { if (this.el.play2) this.el.play2(); },
	/**
	 * pause video
	 */
	pause: function() {	if (this.el.pause) this.el.pause(); },
	/**
	 * seek to specified point in video
	 * @param {int} seconds seconds into video
	 */
	seek: function(seconds) {
		var el = this.el;
		
		if (!isNaN(seconds) && el.canSeekTo && el.canSeekTo(seconds)) {
			el.seek(seconds);
		}
	},
	/**
	 * get current position into video
	 * @returns {int} current time
	 */
	currentTime: function() {
		if (this.el.getCurrentTime) {
			return this.el.getCurrentTime();
		} else {
			return 0;
		}
	},
	/**
	 * get duration of current video
	 * @returns {int} total duration
	 */
	duration: function() {
		if (this.el.getDuration) {
			return this.el.getDuration();
		} else {
			return undefined;
		}
	},
	paused: function() {
		if (this.el.getState) {
			return this.el.getState() === 'paused';
		} else {
			return true;
		}
	},
	unload: function () {
		this.metrics = undefined;
		this._removeEventHandlers();
		this.loaded = false;
		
		if (!this.el) return;
		
		this.pause();
		this.el.src = '';
		$(this.el).remove();
		this.el = undefined;
	},
	
	errorCodes: {
		IO_ERROR: 1,
		SECURITY_ERROR: 2,
		ASYNC_ERROR: 3,
		URL_SCHEME_INVALID: 5,
		HTTP_GET_FAILED: 6,
		MEDIA_LOAD_FAILED: 7,
		SOUND_PLAY_FAILED: 10,
		NETCONNECTION_REJECTED: 11,
		NETCONNECTION_APPLICATION_INVALID: 12,
		NETCONNECTION_FAILED: 13,
		NETCONNECTION_TIMEOUT: 14,
		NETSTREAM_PLAY_FAILED: 15,
		NETSTREAM_STREAM_NOT_FOUND: 16,
		NETSTREAM_FILE_STRUCTURE_INVALID: 17,
		NETSTREAM_NO_SUPPORTED_TRACK_FOUND: 18
	}
});