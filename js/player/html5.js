(function(vboss, $) {

	/**
	 * html5 player
	 */
	vboss.player.html5 = function(applicationId, containerId, options) {
		
		this.applicationId = applicationId;
		this.containerId = containerId;
		this.assetId = null;
		
		
		this.options = $.extend({}, this.defaultOptions, options);
		this.volume = this.options.volume || 1.0;

		// connect to metrics service and initialize session
		this.loaded = false;
		this.metrics = new vboss.metrics(applicationId);
		
		this.$el = this._createVideoElement();
		
		this.metrics.addEventHandlers(this.$el[0]);
		this._addEventHandlers(this.$el);
		
		// swap existing content with video
		// TODO should this go somewhere else in the async chain?
		$('#'+this.containerId).replaceWith(this.$el);
		
		this.sessionRequest = this.metrics.createSession();
		
		// versionSelect keeps track of available quality options
		this.versionSelect = new vboss.player.versionSelect(this.options.theme);
		
		// in instances where only a FLV version is available for a video then fallback to flash
		if(options.flashFallback) {
			// this event is only fired by versionSelect object
			vboss.util.on('flverror', function() {
				var flashPlayer = new vboss.player.flash(applicationId, containerId, this.options);
				flashPlayer.load(this.assetId);
			});
		}
		
		return this;
		
	};

	$.extend(vboss.player.html5.prototype, vboss.player.prototype, {
		seekIncrement: 5.0, // keyboard handler seek amount (in seconds)
		
		/**
		 * load an asset.  
		 * see vboss.player.load for parameters
		 */
		load: function(assetId, options) {
			var self = this,
					$el = this.$el,
					readyPromise = $.Deferred(),
					startTime = 0,
					autoplay, version;
			
			options = $.extend({}, this.options, options);
			
			autoplay = options.autoplay;
			startTime = options.startTime || 0;
			version = options.version;
			
			this.assetId = assetId;
			this.assetUrl = null;
			
			$el.attr('poster', 'http://previews.fliqz.com/' + this.assetId + '.jpg');
			
			$el.css({
				width: options.width,
				height: options.height,
				'background-color': decodeURIComponent(options.bgcolor)
			});
			
			if(options.automute) {
				$el.volume = 0;
			}
			
			this.sessionRequest.then(function() {
				// log metrics load and get default asseturl
				self.metrics.load(self.assetId)
					.done(function(assetUrl) { self.assetUrl = assetUrl; })
					.fail(function(error) {
						self.assetUrl = null;
						readyPromise.reject(error);
					})
					.then($.proxy(function() {
						this.loaded = true;
						
						try {	
							// update the urls for each quality version
							self.versionSelect.load(self.assetId, self.assetUrl);
						} catch (e) {
							vboss.util.handleError(e.message);
							readyPromise.reject(); // error, FLV
							return;
						}
						
						var canPlayPromise = self._updateSource($el, startTime, version);
						
						if(autoplay) {
							canPlayPromise.done(function() {
								self.play();
								readyPromise.resolve();
							})
							.fail(function() {
								readyPromise.reject();
							});
						} else {
							readyPromise.resolve();
						}
						
					}, self));
			});
			
			return readyPromise;

		},
		

		/**
		 * returns active version
		 */
		currentVersion: function() {
			var version = this.versionSelect.current;
			return {
				name: version.name, 
				resolution: version.resolution, 
				bitrate: version.bitrate
			};
		},
		/**
		 * returns a list of available versions based on theme
		 *
		 * @returns {array} array of objects with following format:
		 *		@param {string} name version name (for use with changing current version with player.version() )
		 *		@param {int} resolution the height resolution (i.e. 720p, 360p)
		 *		@param {int} bitrate the approximate bitrate of video
		 *
		 */
		getVersions: function() {
			var out = [];
			for(var i=0, version; version = this.versionSelect.versions[i]; i++) {
				if(version.error) continue;
				
				out.push({
					name: version.name, 
					resolution: version.resolution, 
					bitrate: version.bitrate
				});
			}
			
			return out;
		},
		/**
		 * change the current version quality
		 * switches to a different video version (if possible) and resumes from current point.
		 */
		setVersion: function(versionName) {
			// skip if same version
			if(versionName && (this.versionSelect.currentName == versionName)) {
				vboss.util.handleError('current version is already ' + versionName);
				return;
			}
			
			var self = this,
					$newVideoEl = this._createVideoElement(),
					$oldVideoEl = this.$el,
					canPlayPromise = this._updateSource($newVideoEl, this.currentTime(), versionName);

			canPlayPromise
				.done(function () {
					var swapPromise = $.Deferred(),
							paused = $oldVideoEl[0].paused,
							swapEvent =	paused ? 'suspend' : 'timeupdate';
					
					$newVideoEl[0].play();

					// once new version has buffered or frame is available swap
					$newVideoEl.one('suspend timeupdate', swapPromise.resolve);
					// or swap after 4 seconds if neither occurs
					setTimeout(swapPromise.resolve, 3.5 * 1000);					
					
					swapPromise.done(function() {
						//self.metrics.addEventHandlers($newVideoEl);
						// delay swap slightly to allow buffer to make up for load delay
						setTimeout($.proxy(self, '_swapVideoElements', $newVideoEl, $oldVideoEl), 500);
					});
				})
				.fail(function () {
					vboss.util.handleError('unable to swap version, error occured');
					$newVideoEl.remove();
				});
		},
		_swapVideoElements: function($currentEl, $previousEl) {
			var previousEl = $previousEl[0],
					currentEl = $currentEl[0],
					startTime = previousEl.currentTime;

			this.$el = $currentEl;
			
			var x = $previousEl.replaceWith($currentEl);
			
			this._removeEventHandlers($previousEl);
			previousEl.pause();
			previousEl.src = null;
			$previousEl.remove();

			
			// may have been slight delay while loading, so update to latest
			if(startTime && (startTime > 0)) {
				currentEl.currentTime = startTime;
				currentEl.play(); // firefox misses previous play request
			}
			
			this.metrics.addEventHandlers(currentEl);
			this._addEventHandlers($currentEl);
			
			vboss.util.publish('version', this.versionSelect.current);
		},
		/**
		 * private function to set current src url, time
		 */
		_updateSource: function($el, startTime, versionName) {
			var self = this,
					versionSelect = this.versionSelect,
					el = $el[0],
					canPlayPromise = new $.Deferred();
			
			!isNaN(startTime) || (startTime = 0);
			
			try {
				
				if(versionName) versionSelect.update(versionName);
				if(!versionSelect.currentSrc) throw new Error('no available version to play');
				
			} catch (e) {
			
				vboss.util.handleError(e.message);
				canPlayPromise.reject();
				return canPlayPromise;
				
			} finally {
				el.src = versionSelect.currentSrc;
				
			}
			
			$el.one({
				'loadedmetadata': function() {
					if(startTime) el.currentTime = startTime;
						
				},
				'canplay': function() {
				
					canPlayPromise.resolve();
						
				},
				'error': function() {
				
					canPlayPromise.reject();
					
				}
			});
			
			return canPlayPromise;
		},
		
		/**
		 * create video element and add event handlers
		 */
		_createVideoElement: function() {
			/* init dom */
			var $el = $('<video></video>', {
				id: this.containerId,
				controls: 'controls',
				preload: 'metadata',
				poster: null,
				src: null,
				css: {
					width: this.options.width,
					height: this.options.height
				}
			});
			
			$el[0].volume = this.volume;
			this._addErrorHandler($el);
			
			return $el;
		},
		/**
		 * add event handling to video element
		 */
		_addEventHandlers: function ($el) {
			var containerId = this.containerId,
					el = $el[0];
			
			$el.on('play pause resume ended seeking seeked timeupdate ended', function(evt) {
					var eventName = evt.type,
							details = {};
					
					switch (eventName) {
						case 'timeupdate':
							details.currentTime = el.currentTime;
							details.duration = el.duration;
						break;
					}
					
					vboss.util.publish(eventName, details, containerId);
			});
		},
		_removeEventHandlers: function ($el) {
			$el.off('play pause resume ended seeking seeked timeupdate ended error');
		},
		_addErrorHandler: function($el) {
			var self = this,
					el = ($el || this.$el)[0];
			
			$el.on('error', function(e) {
				var error = el.error;
				if(!error) return vboss.util.handleError('unknown player error');
				switch(error.code) {
					case error.MEDIA_ERR_ABORTED:
						vboss.util.handleError('Video load aborted');
					break;
					case error.MEDIA_ERR_DECODE:
						vboss.util.handleError('Error decoding video');
					break;
					case error.MEDIA_ERR_NETWORK:
						vboss.util.handleError('Error connecting to network');
					break;
					case error.MEDIA_ERR_SRC_NOT_SUPPORTED:
						vboss.util.handleError('src not supported: ' + el.src + ', trying alternate version');
						/**
						 * on 'not supported' error (404 not found or browser can't play)
						 * skip current version and load next available version
						 */
						 self.versionSelect.failCurrent();
						
						// autoplay, resuming from current position in video
						self.setVersion();
						
					break;
				}
				
			});
			
			// prevent context menu
			$el.on('contextmenu', function() { return false; });
			
			// allows keyboard control after clicking on video (can also focus via tab)
			$el.click(function() { $el.focus(); });
			
			// attach keyboard event handlers
			var _tempStoredVolume = 0; // used for mute/unmute
			$el.on('keydown', function(evt) {
				var seekIncrement = 5.0; // seek forward/back at 5 second intervals
				
				switch(evt.which) {
					case 32:	// space - play/pause
						if(el.paused || el.ended) {
							el.play();
						} else {
							el.pause();
						}
						break;
					case 38: // up - volume up
						el.volume = Math.min(el.volume + 0.1, 1.0);
						break;
					case 40: // down - volume down
						el.volume = Math.max(el.volume - 0.1, 0.0);
						break;
					case 77: // m - mute/unmute
					case 109: 
						if(el.volume > 0) {
							_tempStoredVolume = el.volume;
							el.volume = 0;
						} else {
							el.volume = (_tempStoredVolume) ? _tempStoredVolume : 1.0;
						}
							
						break;
					case 37: // left - rewind
						if(!isNaN(el.duration) && (el.duration > 0)) {
							
							// seek 5 seconds back
							el.currentTime = Math.max(el.currentTime - self.seekIncrement, 0.0);
						}
						break;
					case 39: // right - fast forward
						if(!isNaN(el.duration) && (el.duration > 0) && !el.ended) {
							// seek 5 seconds forward
							el.currentTime = Math.min(el.currentTime + self.seekIncrement, el.duration);
						}
						break;
					case 70: // F, fullscreen, not compatible with all browsers
					case 102: // f, fullscreen
						if(el.requestFullScreen) {
							el.requestFullScreen();
						} else if(el.mozRequestFullScreen) {
							el.mozRequestFullScreen();
						} else if(el.webkitRequestFullScreen) {
							el.webkitRequestFullScreen();
						}
						break;
					default:
						break;
				};
			
				evt.preventDefault(); // prevent bubble
			});
			
			$el.on('volumechange', function(evt) {
				self.volume = el.volume;
			});
						
		},
		/* event control */
		setVolume: function(newVolume) {
			var el = this.$el[0],
					volume = !isNaN(newVolume) ? newVolume : this.volume;
			
			volume = Math.min(Math.max(newVolume, 0), 1.0);
			el.volume = volume;
			this.volume = volume;
			return this;
		},
		getVolume: function() { return this.$el[0].volume; },
		/**
		 * play video
		 */
		play: function() { this.$el[0].play(); },
		/**
		 * pause video
		 */
		pause: function() {	this.$el[0].pause(); },
		/**
		 * seek to specified point in video
		 * @param {int} seconds seconds into video
		 */
		seek: function(seconds) {
			var el = this.$el[0];
			
			if(isNaN(seconds)) return;
			el.currentTime = Math.min(Math.max(seconds, 0), el.duration);
		},
		/**
		 * get current position into video
		 * @returns {int} current time
		 */
		currentTime: function() { return this.$el[0].currentTime; },
		/**
		 * get duration of current video
		 * @returns {int} total duration
		 */
		duration: function() { return this.$el[0].duration; },
		paused: function() { return this.$el[0].paused; },
		/** 
		 * add event handlers convenience function.  supports all html5 player events
		 * (play, pause, ended, timeupdate)
		 * @param {string} eventName name of event
		 * @param {function} callback function to run on event.
		 */
		on: function(eventName, callback) {
			if (typeof console !== 'undefined' && console.warn) {
				console.warn('WARNING: vboss.player.html5.on method of attaching event handlers is depreciated, as it is not compatible with automatic bitrate switching');
			}
			return this.$el.on(eventName, callback);
		},
		// stop playback and remove dom content
		unload: function() {
			this.loaded = false;
			this.metrics = undefined;
			if (!this.$el) return;
			
			this._removeEventHandlers(this.$el);
			this.pause();
			this.$el[0].src = '';
			this.$el.remove();
			this.$el = undefined;
		}
	});

})(vboss, $);