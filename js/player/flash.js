/**
 * wrapper around vboss flash player
 */
vboss.player.flash = function(applicationId, containerId, options) {
	var _this = this,
			protocol = /https/.test(document.location.protocol) ? 'https:' : 'http:';
	
	this.applicationId = applicationId.replace(/-/g, '');
	this.containerId = containerId;
	this.assetId = null;
	
	this.data = { currentTime: 0, lastTimeUpdate: null };
	
	this.options = $.extend({}, this.defaultOptions, options);
	this.paused = true;
	this.ended = false;
	this.loaded = false;
	
	// placeholder to avoid undefined errors
	this.el = {};
	
	this.addEventHandlers();
	
	// load swfobject library from CDN if it doesn't already exist
	this.whenSWFObjectLoaded = (typeof swfobject != 'undefined' && swfobject)
		? $.when(swfobject) // immediate
		: $.getScript(protocol + '//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js');

	return this;
};

$.extend(vboss.player.flash.prototype, vboss.player.prototype, {
	/**
	 * load an asset.  uses SWFobject
	 * see vboss.player.load for parameters
	 */
	load: function(assetId, options) {
		var _this = this,
				containerId = this.containerId,
				whenPlayerLoaded = new $.Deferred(); // tell listeners that player is available
		
		options = $.extend({}, this.options, options);
	
		this.assetId = assetId.replace(/-/g, '');
		// waits for SWFObject to be available if it isn't already
		this.whenSWFObjectLoaded.then(function() {
			swfobject.embedSWF(
				'http://applications.fliqz.com/' + _this.applicationId + '.swf', // swf
				containerId, // target div id
				options.width, // width
				options.height, // height
				'9.0.115', // version
				null, // express install
				{
					at: assetId,
					autoPlayback: options.autoplay,
					audioMute: options.automute
				}, // flashvars
				{
					wmode: options.wmode,
					bgcolor: decodeURIComponent(options.bgcolor),
					allowscriptaccess: 'always',
					allowfullscreen: 'true',
					allownetworking: 'all'
				}, // params
				{	name: containerId	}, // attributes
				function() { // callback function
					whenPlayerLoaded.resolve();
				}
			);
		});
		
		whenPlayerLoaded.done(function() {
			_this.el = document.getElementById(containerId);
			_this.loaded = true;
			vboss.util.publish('loadedmetadata');
		});
		
		return whenPlayerLoaded;
	},
	
	addEventHandlers: function() {
		var _this = this;
		
		// add event handler for notification that control surface is enabled
		vboss.util.on('flashReady', function(objectId, duration, bytes) {
			// see if this is the same player instance
			if(_this.containerId == objectId) {
				_this.data.duration = duration;
				$(_this).trigger('ready');
			}
		});
		
		vboss.util.on('play', function(assetId) {
			if(assetId && assetId.replace(/-/g,'') != _this.assetId) return;
			_this.paused = false;
			_this.ended = false;
			
			_this.data.lastTimeUpdate = Date.now();
		});
		
		vboss.util.on('pause', function(assetId) {
			if(assetId && assetId.replace(/-/g,'') != _this.assetId) return;
			
			if(!_this.paused) {
				var now = Date.now();
				var prev = _this.data.lastTimeUpdate || now;
				var diff = (now - prev) / 1000;
				_this.data.currentTime += diff;
				_this.data.lastTimeUpdate = now;
			}
			
			_this.paused = true;
		});
		
		vboss.util.on('resume', function(assetId) {
			if(assetId && assetId.replace(/-/g,'') != _this.assetId) return;
			_this.paused = false;
		});
		
		vboss.util.on('ended', function(assetId) {
			if(assetId && assetId.replace(/-/g,'') != _this.assetId) return;
			_this.ended = true;
			_this.data.currentTime = _this.data.duration;
		});
		
		vboss.util.on('timeupdate', function(assetId, time, percent) {
			if(assetId && assetId.replace(/-/g,'') != _this.assetId) return;
			
			_this.data.currentTime = parseFloat(time);
			_this.data.lastTimeUpdate = Date.now();
		});
	},
	
	play: function() {
		if(this.el.play) 
			this.el.play();
		else if(this.el.Update)
			this.el.Update('play.start');
	},
	
	pause: function() {
		if(this.el.pause) this.el.pause();
	},
	paused: function() {
		return this.paused;
	},
	seek: function(seconds) {
		if(this.el.seek) this.el.seek(seconds);
		this.updateCurrentTime(); // update current time
	},
	
	volume: function(volume) {
		if(volume == undefined) return this.data.volume;
		this.data.volume = volume;
		if(this.muted && volume > 0) this.muted = false;
		
		if(this.el.volume) this.el.volume(volume);
	},
	setVolume: function (volume) { this.volume.call(this, volume); },
	getVolume: function (volume) { this.volume.call(this, volume); },
	
	mute: function(muted) {
		if(muted || typeof muted == 'undefined') {
			this.data._lastVolume = this.data.volume || 0.5;
			this.muted = true;
			this.volume(0);
		} else {
			this.muted = false;
			this.volume(this.data._lastVolume || 1.0);
		}
	},
	
	controls: function(showControls) {
		if(this.el.controls)
			this.el.controls( !!showControls ); // convert to boolean
	},
	
	/**
	 * removes seek ability from control surface.
	 */
	disableSlider: function() {
		if(this.el.disableSlider) this.el.disableSlider();
	},
	
	/**
	 *
	 */
	duration: function() { return this.data.duration; },
	currentTime: function() { 
		if(this.paused) return this.data.currentTime;
		
		var now = Date.now();
		var prev = this.data.lastTimeUpdate || now;
		var diff = (now - prev) / 1000;
		
		this.updateCurrentTime(); // make sure updated in case of seek
				
		return this.data.currentTime + diff;
	},
	buffered: function() { return this.data.buffered; },	
	
	/**
	 * gets duration. async, so must use callback
	 */
	updateDuration: function(callback) {
		var _this = this;
		
		if(!this.el.duration) return;
		var tempName = this._asyncFlashCallback(function(duration) {
			_this.data.duration = parseFloat(duration);
			if(callback) callback(duration);
		});
		this.el.duration(tempName);
	},
	
	/**
	 * gets current time.  async, so must use callback
	 */
	updateCurrentTime: function(callback) {
		var _this = this;
		
		if(!this.el.currentTime) return;
		var tempName = this._asyncFlashCallback(function(_time) {
			var time = parseFloat(_time, 10);
			if(!isNaN(time)) {
				_this.data.currentTime = parseFloat(time, 10);
				_this.data.lastTimeUpdate = Date.now();
			}
			
			if(callback) callback(time);
		});
		this.el.currentTime(tempName);
	},
	
	/**
	 * gets percent of video buffered, as number between 0 and 1.  async, so must use callback
	 */
	updateBufferedPercent: function(callback) {
		var _this = this;
		
		if(!this.el.bufferedPercent) return;
		var tempName = this._asyncFlashCallback(function(pct) {
			_this.data.buffered = pct;
			if(callback) callback(pct);
		});
		this.el.bufferedPercent(tempName);
	},
	
	
	/**
	 * private function.  creates temporary variable for registering callbacks for
	 * flash functions
	 */
	_asyncFlashCallback: function(callback) {
		// store callback with temporary name in global namespace (flash doesn't know context)
		var tmpName = 'callback' + (Math.random()*0x10000000|0).toString(16);
		if(!vboss.util.temp) vboss.util.temp = {}; 
		// on retrieved duration delete temporary variable
		vboss.util.temp[tmpName] = function(duration) {
			callback(duration);
			delete vboss.util.temp[tmpName];
		};
		
		return "vboss.util.temp['" + tmpName + "']";
	},
	unload: function () {
		$(this.el).remove();
		this.loaded = false;
	}
	
	/**
	 *
	 */
	
});
