/**
 * versionSelect is used for managing the multiple versions of each asset
 * once uploaded to the VBOSS services a video is encoded to multiple versions,
 * each at different bitrates/resolutions/settings to target a particular resolution
 * or device compatibility.  This class is used to load a specified version.
 * If an error occurs while loading a version (set via versionSelect.failCurrent()) then it is skipped
 * and the current version is set to the next available.
 *
 * Usage:
 * // create instance
 * versionSelect = new vboss.versionSelect('standard');
 * 
 * // load asset via ID, and default URL (if available).  
 * // initializes to use highest quality version available
 * versionSelect.load(assetId, defaultUrl);
 * url = versionSelect.currentSrc;
 *
 * // set to specified quality level, retrieve current URL
 * url = versionSelect.setByName('mobile').currentSrc;
 * // or set by preferred resolution
 * url = versionSelect.setByResolution(360).currentSrc;
 *  
 * NOTE: each VBOSS account is configured with an encoding 'theme' that specifies
 * what versions are created for each asset.  The most common one is our 'standard' encoding.
 * If your account is configured to a different setting then initialize the class with a different theme.
 * Check with VBrick Support for your account's settings.
 *
 */
(function(vboss, $) {
	var BASE_URL = 'http://cdn9.fliqz.com/';
	
	var CLASSIFICATIONS = {
		screencap: {
			name: 'screencap', id: 'afc286b7786a43f68aeacdff286b0722',
			type: "video/mp4; codecs='avc1.64401F, mp4a.40.5'",
			resolution: 1080, bitrate: 400
		},
		hd: { // hd 720p
			name: 'hd', id: '827283d4f0a4475f9ca40b94417ced0e',
			type: "video/mp4; codecs='avc1.64401F, mp4a.40.5'",
			resolution: 720, bitrate: 2100
		},
		hq: { // 540p appletv/iPad compatible
			name: 'hq', id: 'f3947812b8f8401eb6097d4ac9ecf98b',
			type: "video/mp4; codecs='avc1.4D401F, mp4a.40.5'",
			resolution: 540, bitrate: 1500
		},
		sd: { // 360p standard
			name: 'sd', id:'e8763a88ea7d443794ac94c4379f2df7',
			type: "video/mp4; codecs='avc1.64001F, mp4a.40.5'",
			resolution: 360, bitrate: 800
		}, 
		mobile: { // 360p mobile medium quality
			name: 'mobile', id: '0f849b15cfb6401ba59ba38584249412',
			type: "video/mp4; codecs='avc1.42E016, mp4a.40.2'",
			resolution: 320, bitrate: 700
		},
		mobilelow: { // 240p low bitrate mobile
			name: 'mobilelow', id:	'2872eaf6dc6643fb8c80e66038b64084',
			type: "video/mp4; codecs='avc1.42E016, mp4a.40.2'",
			resolution: 240, bitrate: 500
		},
		lowest: { // 180p lowest possible
			name: 'lowest', id: '773c95496a384ef6a6155d61697c9d8e',
			type: "video/mp4",
			resolution: 180, bitrate: 300
		}
	};
	
	var THEMES = {
		standard: ['sd','mobile','mobilelow'],
		balanced: ['hd','sd','mobilelow'],
		screen: ['screencap','sd','mobilelow'],
		hd: ['hd','hq','sd'],
		vems: ['hd','sd','mobile'],
		settop: ['hq','mobile','mobilelow'],
		mobile: ['mobile', 'mobilelow', 'lowest']
	};
	
	
	var versionSelect = function(theme) {
		
		theme = $.isArray(theme)
			? theme
			: THEMES[theme] || THEMES['standard'];
		
		var videoElement = document.createElement('video'),
		// firefox reports "no" if codec is specified, so check if it can play MP4 but not lowest quality baseline
				isBrowserMisreportingCodecSupport = videoElement.canPlayType("video/mp4") && !videoElement.canPlayType("video/mp4; codecs='avc1.42E016, mp4a.40.2'");
		
		this.assetId = null;
		this.current = null;
		this.currentSrc = null;
		this.currentName = null;
		
		// include versions in theme if browser supports type
		this.versions = [];
		for(var i=0, name; name=theme[i]; i++) {
			var version = $.extend({
					url: null,
					error: false
				}, CLASSIFICATIONS[name]);
				
			// only add version to list if browser supports it
			var canPlay = videoElement.canPlayType(version.type);
			if(isBrowserMisreportingCodecSupport || canPlay.replace(/no/,'')) this.versions.push(version);
		}
		
		videoElement = null;
		
		// include default version
		this['default'] = {
			name: 'default', id: 'default',
			type: "video/mp4",
			resolution: 0, bitrate: 0,
			error: false,
			url: null
		};
		
		// change to use default first
		//this.versions.push(this['default']);
		this.versions.unshift(this['default']);
		
		return this;
	};
	
	/**** functions ***/
	
	// initialize with asset information		
	versionSelect.prototype.load = function(assetId, defaultUrl) {
		this.assetId = assetId.replace(/-/g,'');
		
		if(defaultUrl && defaultUrl.match(/\.flv/)) {
			vboss.util.publish('flverror');
			throw new Error('FLV playback via HTML5 not supported');
		}
		
		// set url for each version
		for(var i=0, version; version = this.versions[i]; i++) {
			version.error = null; // reset all errors
			version.url = this.generateVersionUrl(version, assetId, defaultUrl);
		}
		
		this.update();
		return this;
	};
	versionSelect.prototype.generateVersionUrl = function (version, assetId, defaultUrl) {
		var baseUrl = BASE_URL + assetId + '.mp4';
		if (version.name === 'default') {
			return defaultUrl || baseUrl;
		} else {
			return baseUrl + '?acl=' + version.id;
		}
	};
	// set current version to one specified by name, or first available
	versionSelect.prototype.update = function(name) {
		var version = (name) 
			? this.getVersionByName(name)
			: this.getFirstAvailableVersion();
		
		if (version == undefined) {
			// add if version not in theme
			if (CLASSIFICATIONS[name]) {
				version = $.extend({}, CLASSIFICATIONS[name]);
				version.error = null;
				version.url = this.generateVersionUrl(version, assetId);
				this.versions.push(version);
			} else {
				this.current = null;
				this.currentSrc = null;
				this.currentName = null;
				throw new Error('no available version found');
			}
		}
		
		this.current = version;
		this.currentSrc = version.url;
		this.currentName = version.name;
		
		return this;
	};
	
	// set current version by name
	versionSelect.prototype.setByName = function(name) { 
		this.update(name); 
		return this; 
	};
	
	// set current version by preferred resolution.  if no exact match found returns closest option
	versionSelect.prototype.setByResolution = function(resolution) {
		resolution = parseInt(resolution);
		var closest = this.versions[0];
		var diff = Math.abs(closest.resolution - resolution);
		
		var match = this.versions.filter(function(version) {
			if(version.resolution==resolution) 
				return true;
			else if(Math.abs(version.resolution-resolution) < diff) {
				diff = Math.abs(version.resolution-resolution);
				closest = version;
			}
			return false;
		});
		
		if(match.length) this.update(match[0].name);
		else this.update(closest.name);
		
		return this;
	};
	
	// return version specified by name, or undefined if not found
	versionSelect.prototype.getVersionByName = function(name) {
		return this.versions.filter(function(version) {	
			return version.name == name; 
		})[0];
	};
	
	// return first version that hasn't failed
	versionSelect.prototype.getFirstAvailableVersion = function() {
		return this.versions.filter(function(version) {
			return !version.error;
		})[0];
	};
	
	// called on error with current src, load alternate
	versionSelect.prototype.failCurrent = function() {
		if(this.current) this.current.error = true;
		this.update();
	};
	
	vboss.player.versionSelect = versionSelect;

	return vboss;

})(vboss, $);